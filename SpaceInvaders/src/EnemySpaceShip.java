import java.awt.Image;
import javax.swing.ImageIcon;

//class containing information about Enemy Spaceships
public class EnemySpaceShip extends Object {

	private Image enemySpaceShipImage; //enemySpaceShip is an object of type Image
	public EnemyMissiles missile; //missile is an object of type EnemyMissiles
	public PowerUps powerUp; //powerUp is an object of type PowerUps
	private String missileImageLocation = "EnemyMissile.png";
	private String powerUpImageLocation = "PowerUp.png";
	private float xPosition; //x position
	private float yPosition; //y position
	private double xVelocity; //velocity of x
	private double yVelocity; //velocity of y
	private int xVelocityChanges = 0; //tracks number of times x velocity changes
	private boolean visible = true; //visibility
	public int minXPosition = 5; //minimum x position
	public int maxXPosition = 745; //maximum x position
	private int hitCount; // to keep track of how many times it has been hit by the player

	//constructor for EnemySpaceShip class
	public EnemySpaceShip(Image enemy) {
		this.enemySpaceShipImage = enemy;
		hitCount = 0;

	}

	public void createMissile() {
		Image missileImage = new ImageIcon(missileImageLocation).getImage(); //creating missileImage
		EnemyMissiles enemyMissile = new EnemyMissiles(missileImage); //creating object missile which is type Missiles
		this.missile = enemyMissile;
	}

	public void createPowerUp() {
		Image powerUpImage = new ImageIcon(powerUpImageLocation).getImage(); //creating powerUpImage
		PowerUps powerUp = new PowerUps(powerUpImage); //creating object missile which is type Missiles
		this.powerUp = powerUp;
	}

	//updates position of spaceship
	public void updatePosition() {
		if (xPosition < minXPosition || xPosition > maxXPosition) {
			xVelocity = -xVelocity; //if it meets the above criteria reverse its direction
			xVelocityChanges++; //keeping a track of the number of times the x velocity has changed
		}

		xPosition += xVelocity; //moving the spaceship in the x direction
		yPosition += yVelocity; //moving the spaceship in the y direction
	}

	//returns x position of spaceship
	public float getXPosition() {
		return xPosition;
	}

	//returns y position of spaceship
	public float getYPosition() {
		return yPosition;
	}

	//set x position of spaceship
	public void setXPosition(float x) {
		this.xPosition = x; //the xPosition of the spaceship equals the value of x passed
	}

	//set y position of spaceship
	public void setYPosition(float y) {
		this.yPosition = y; //the yPosition of the spaceship equals the value of y passed
	}

	//get width of the image
	public int getWidth(){
		return enemySpaceShipImage.getWidth(null); //returns the width of the image in pixels
	}

	//get height of the image
	public int getHeight(){
		return enemySpaceShipImage.getHeight(null); //returns the height of the image in pixels
	}

	//returns horizontal velocity (xVelocity)
	public double getXVelocity() {
		return xVelocity;
	}

	//returns vertical velocity (yVelocity)
	public double getYVelocity() {
		return yVelocity;
	}

	//set horizontal velocity (xVelocity)
	public void setXVelocity(double xv) {
		this.xVelocity = xv;
	}

	//set vertical velocity (yVelocity)
	public void setYVelocity(double yv) {
		this.yVelocity = yv;
	}

	//get visibility value
	public boolean getVisibility() {
		return visible;
	}

	//set visibility value
	public void setVisibility(boolean b) {
		this.visible = b;
	}

	//get image of enemy
	public Image getImage(){
		return enemySpaceShipImage;
	}
	
	//get the number of times hit by the player
	public int getHitCount(){
		return hitCount;
	}
	
	//adds a hit if the enemy is hit by the player's missile
	public void setHitCount(int hit){
		hitCount = hitCount + hit;
	}
	
	//returns how many times the x velocity has changed for the spaceship
	public int getXVelocityChanges() {
		return xVelocityChanges;
	}

	//set the number of times the x velocity has changed for the spaceship
	public void setXVelocityChanges(int x) {
		this.xVelocityChanges = x; //the xVelocityChanges of the spaceship equals the value of x passed
	}
}