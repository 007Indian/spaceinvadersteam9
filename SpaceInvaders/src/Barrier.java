import java.awt.Image;

//class containing information about Enemy Spaceships
public class Barrier {

	private Image barrierImage; //barrierImage is an object of type Image
	private int xPosition; //x position
	private int yPosition; //y position
	private boolean visible = true; //visibility
	public int minXPosition = 5; //minimum x position
	public int maxXPosition = 745; //maximum x position

	//constructor for Barrier class
	public Barrier(Image image) {
		this.barrierImage = image;
	}

	//returns x position of barrier
	public int getXPosition() {
		return xPosition;
	}

	//returns y position of barrier
	public int getYPosition() {
		return yPosition;
	}

	//set x position of barrier
	public void setXPosition(int x) {
		this.xPosition = x; //the xPosition of the barrier equals the value of x passed
	}

	//set y position of barrier
	public void setYPosition(int y) {
		this.yPosition = y; //the yPosition of the barrier equals the value of y passed
	}

	//get width of the image
	public int getWidth(){
		return barrierImage.getWidth(null); //returns the width of the image in pixels
	}

	//get height of the image
	public int getHeight(){
		return barrierImage.getHeight(null); //returns the height of the image in pixels
	}

	//get visibility value
	public boolean getVisibility() {
		return visible;
	}

	//set visibility value
	public void setVisibility(boolean b) {
		this.visible = b;
	}

	//get image of barrier
	public Image getImage(){
		return barrierImage;
	}
}