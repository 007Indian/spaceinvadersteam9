import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;


public class OptionScreenFromWelcomeScreen extends JPanel implements ActionListener, ItemListener{

	GameControl state = new GameControl();

	private static final long serialVersionUID = 1L;
	JToggleButton track1toggle;
	JToggleButton track2toggle;
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	JLabel optionLabel2;
	public OptionScreenFromWelcomeScreen(){

		this.setBackground(Color.BLACK); 

		JLabel background=new JLabel(new ImageIcon("Stars.png"));

		background.setLayout(new GridBagLayout());
		GridBagConstraints OptionScreenFromWelcomeScreenContraint = new GridBagConstraints();
		OptionScreenFromWelcomeScreenContraint.insets = new Insets(0,0,20,0);
		
		//CREATING ALL THE COMPONENTS NEEDING TO BE ADDED TO HE BACKGROUND
		track1toggle = new JToggleButton("Turn Track 1 On/Off");
		track1toggle.setToolTipText("If Track 1 is playing press the button twice");
		track1toggle.setFont(new Font("Aharoni", Font.BOLD, 26));
		track1toggle.addItemListener(this);
		
		track2toggle = new JToggleButton("Turn Track 2 On/Off");
		track2toggle.setToolTipText("If Track 2 is playing press the button twice");
		track2toggle.setFont(new Font("Aharoni", Font.BOLD, 26));
		track2toggle.addItemListener(this);

		JLabel optionLabel1 = new JLabel("<html> OPTIONS <br><br><html>");

		optionLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		optionLabel1.setForeground(Color.WHITE);

		optionLabel2 = new JLabel("<html>Pick a track to play or stop<br><br><br><br> ");
		optionLabel2.setFont(new Font("Aharoni", Font.PLAIN, 26));
		optionLabel2.setForeground(Color.WHITE);

		OptionScreenFromWelcomeScreenContraint.gridx = 0;
		OptionScreenFromWelcomeScreenContraint.gridy = 0;
		
		//THE FOLLOWING CODE IS USED TO ADD ALL THE COMPONENTS TO THE BACK GROUND 
		//background.add
		background.add(optionLabel1,OptionScreenFromWelcomeScreenContraint);
		
		//these control the position the components are being placed
		OptionScreenFromWelcomeScreenContraint.gridx = 0;
		OptionScreenFromWelcomeScreenContraint.gridy = 3;
		background.add(optionLabel2,OptionScreenFromWelcomeScreenContraint);
		
		OptionScreenFromWelcomeScreenContraint.gridx = 0;
		OptionScreenFromWelcomeScreenContraint.gridy = 1;
		background.add(track1toggle,OptionScreenFromWelcomeScreenContraint);
		
		OptionScreenFromWelcomeScreenContraint.gridx = 0;
		OptionScreenFromWelcomeScreenContraint.gridy = 2;
		background.add(track2toggle,OptionScreenFromWelcomeScreenContraint);

		JButton button14 = new JButton("Main Menu");
		button14.setFont(new Font("Aharoni", Font.BOLD,30));
		button14.addActionListener(this);

		OptionScreenFromWelcomeScreenContraint.gridx = 0;
		OptionScreenFromWelcomeScreenContraint.gridy = 4;
		background.add(button14,OptionScreenFromWelcomeScreenContraint);
		button14.setActionCommand("Main Menu");

		this.add(background);
		validate();
	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		
		//this is used when you pressed the JToggle button to check if it is pressed and then plays a sound 
		if (track1toggle.isSelected()){
			try {
				Sounds.playClip1();
			} catch (UnsupportedAudioFileException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (LineUnavailableException e1) {
				e1.printStackTrace();
			}
		}
		else {	

			Sounds.stopClip1();

		}	
		 if (track2toggle.isSelected()){
				try {
					Sounds.playClip2();
				} catch (UnsupportedAudioFileException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (LineUnavailableException e1) {
					e1.printStackTrace();
				}
		}
		else {	
				Sounds.stopClip2();
		}	
	}
	
	//when main menu is pressed it changes to the required screen
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Main Menu"))){
			Screens.goToMainMenu();

			state.welcomeScreenEntered(true);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);

	
		}		
	}

}

