import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class PowerUpScreenFromWelcomeScreen extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();

	public PowerUpScreenFromWelcomeScreen(){
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints helpScreenContraint = new GridBagConstraints();
		helpScreenContraint.insets = new Insets(0,0,7,0);


		//CREATING THE COMPONENTS THAT NEED TO BE ADDED TO THE BACKGROUND
		JLabel helpLabel1 = new JLabel("<html> POWER-UP  <html>", JLabel.CENTER);

		helpLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel1.setForeground(Color.WHITE);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 0;

		background.add(helpLabel1,helpScreenContraint);


		JLabel helpLabel6 = new JLabel("INFORMATION");
		JLabel helpLabel7 = new JLabel("<html><br>The following power ups are attainable at random if the super spaceship is shot: ");
		JLabel helpLabel8 = new JLabel("1. The rate at which the enemies shoot will decrease");
		JLabel helpLabel9 = new JLabel("2. The number of lives remaining for the player will increase");
		JLabel helpLabel10 = new JLabel("3. The number of lives remaining for the player will decrease");
		JLabel helpLabel11 = new JLabel("4. The player's horizontal speed will increase");
		JLabel helpLabel12 = new JLabel("5. The player's horizontal speed will decrease");
		JLabel helpLabel13 = new JLabel("6. The rate at which enemies can shoot is increased");
		JLabel helpLabel14 = new JLabel("<html>7. The superEnemy appear more often<br><br>");

		helpLabel6.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel6.setForeground(Color.WHITE);

		//ADDING ALL THE COMPONENTS TO THE BACKGROUND OF THE PANEL
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 2;

		background.add(helpLabel6,helpScreenContraint);

		helpLabel7.setForeground(Color.WHITE);
		helpLabel7.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 7;

		background.add(helpLabel7,helpScreenContraint);	

		helpLabel8.setForeground(Color.YELLOW);
		helpLabel8.setFont(new Font("Arial", Font.PLAIN, 20));
		
		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 8;

		background.add(helpLabel8,helpScreenContraint);	

		helpLabel9.setForeground(Color.GREEN);
		helpLabel9.setFont(new Font("Arial", Font.PLAIN, 20));
		
		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 9;

		background.add(helpLabel9,helpScreenContraint);	

		helpLabel10.setForeground(Color.PINK);
		helpLabel10.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 10;

		background.add(helpLabel10,helpScreenContraint);	

		helpLabel11.setForeground(Color.CYAN);
		helpLabel11.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 11;

		background.add(helpLabel11,helpScreenContraint);	

		helpLabel12.setForeground(Color.MAGENTA);
		helpLabel12.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 12;

		background.add(helpLabel12,helpScreenContraint);	

		helpLabel13.setForeground(Color.RED);
		helpLabel13.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 13;

		background.add(helpLabel13,helpScreenContraint);

		helpLabel14.setForeground(Color.ORANGE);
		helpLabel14.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting the posisiton 
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 14;

		background.add(helpLabel14,helpScreenContraint);

		//creating the back button and giving it a constraint to apply the placement on the background
		JButton button5 = new JButton("Back");
		button5.setFont(new Font("Aharoni", Font.BOLD, 30));
		button5.addActionListener(this); 
		button5.setForeground(navyBlue);
		button5.setPreferredSize(new Dimension(175, 50));
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 16;
		background.add(button5,helpScreenContraint);
		button5.setActionCommand("Back");

		this.add(background);
		
		//Validating a container means laying out its subcomponents. Layout-related changes, such as setting the bounds of a component, 
		//or adding a component to the container, invalidate the container automatically.
		validate();
	}

	//when the back button is pressed it goes to the main menu
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Back"))){
			Screens.goToHelpScreenFromWelcomeScreen();

		}


	}
}




