import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class HelpScreenFromWelcomeScreen extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	Color bottomOfblue = new Color(40,64,87);
	GameControl state = new GameControl();

	public HelpScreenFromWelcomeScreen(){
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints helpScreenContraint = new GridBagConstraints();
		helpScreenContraint.insets = new Insets(0,0,20,0);

		//CREAING ALL THE COMPONENTS TO BE PLACED ONTO THE BACK GROUND
		JLabel helpLabel1 = new JLabel("<html> HELP!<br> <html>", JLabel.CENTER);

		helpLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel1.setForeground(Color.WHITE);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 0;

		background.add(helpLabel1,helpScreenContraint);

		JLabel helpLabel2 = new JLabel("<html><br><br>Get help by pressing the buttons below, to find out more information");

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 2;

		helpLabel2.setForeground(Color.WHITE);
		helpLabel2.setFont(new Font("Arial", Font.BOLD, 20));

		background.add(helpLabel2,helpScreenContraint);	

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 3;
		
		JButton howToPlayButton = new JButton("Menu Navigation");
		howToPlayButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		howToPlayButton.addActionListener(this); 
		howToPlayButton.setForeground(navyBlue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 4;
		background.add(howToPlayButton,helpScreenContraint);

		howToPlayButton.setActionCommand("Menu Navigation");
		
		JButton powerUpButton = new JButton("Power Up Information");
		powerUpButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		powerUpButton.addActionListener(this); 
		powerUpButton.setForeground(navyBlue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 5;
		background.add(powerUpButton,helpScreenContraint);

		powerUpButton.setActionCommand("Power Up Information");
		
		JButton enemyInformationButton = new JButton("Enemy Information");
		enemyInformationButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		enemyInformationButton.addActionListener(this); 
		enemyInformationButton.setForeground(navyBlue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 6;
		background.add(enemyInformationButton,helpScreenContraint);

		enemyInformationButton.setActionCommand("Enemy Information");
		
		JButton controlsButton = new JButton("Controls");
		controlsButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		controlsButton.addActionListener(this); 
		controlsButton.setForeground(bottomOfblue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 7;
		background.add(controlsButton,helpScreenContraint);

		controlsButton.setActionCommand("Controls");

		JButton backButton = new JButton("Main Menu");
		backButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		backButton.addActionListener(this); 
		backButton.setForeground(bottomOfblue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 8;
		background.add(backButton,helpScreenContraint);

		backButton.setActionCommand("Main Menu");

		this.add(background);
		validate();
	}

	//Letting the screen know that if one of these buttons are pressed then to go the that respectful screen
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Main Menu"))){
			Screens.goToMainMenu();

			state.welcomeScreenEntered(true);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);

		}
		else if ((name.getActionCommand().equals("Menu Navigation"))){
			Screens.goToGameNavigationScreen();
		}
		else if ((name.getActionCommand().equals("Power Up Information"))){
			Screens.goToPowerUpScreenFromWelcomeScreen();

		}
		else if ((name.getActionCommand().equals("Enemy Information"))){
			Screens.goToEnemyInformationScreen();
		}
		else if ((name.getActionCommand().equals("Controls"))){
			Screens.goToControlsHelpScreen();
		}
	}
}




