import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

public class OptionScreenFromGameScreen extends JPanel implements ActionListener, ItemListener{

	private static final long serialVersionUID = 1L;
	GameControl state = new GameControl();
	JLabel optionLabel2;
	JToggleButton track1toggle;
	JToggleButton track2toggle;
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue

	public OptionScreenFromGameScreen(){

		this.setBackground(Color.BLACK); 

		JLabel background=new JLabel(new ImageIcon("Stars.png"));

		background.setLayout(new GridBagLayout());
		GridBagConstraints OptionScreenFromGameScreenContraint = new GridBagConstraints();
		OptionScreenFromGameScreenContraint.insets = new Insets(0,0,20,0);

		track1toggle = new JToggleButton("Turn Track 1 On/Off");
		track1toggle.setToolTipText("If Track 1 is playing press the button twice");
		track1toggle.setFont(new Font("Aharoni", Font.BOLD, 26));
		track1toggle.addItemListener(this);
		
		track2toggle = new JToggleButton("Turn Track 2 On/Off");
		track2toggle.setToolTipText("If Track 2 is playing press the button twice");
		track2toggle.setFont(new Font("Aharoni", Font.BOLD, 26));
		track2toggle.addItemListener(this);
		
		this.setPreferredSize(new Dimension(800, 600));
		this.setLayout(new FlowLayout(FlowLayout.CENTER));


		JLabel optionLabel1 = new JLabel("<html>OPTIONS<br><br><html>", JLabel.CENTER);

		optionLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		optionLabel1.setForeground(Color.WHITE);

		optionLabel2 = new JLabel("<html>Pick a track to play or stop<br><br>", JLabel.LEFT);
		optionLabel2.setFont(new Font("Aharoni", Font.PLAIN, 26));
		optionLabel2.setForeground(Color.WHITE);

		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 0;
		background.add(optionLabel1, OptionScreenFromGameScreenContraint);

		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 4;
		background.add(optionLabel2, OptionScreenFromGameScreenContraint);

		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 2;
		background.add(track1toggle, OptionScreenFromGameScreenContraint);
		
		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 3;
		background.add(track2toggle, OptionScreenFromGameScreenContraint);

		JButton button8 = new JButton("Main Menu");
		JButton button13 = new JButton("Back to Game");
		button8.setFont(new Font("Aharoni", Font.BOLD, 30));
		button13.setFont(new Font("Aharoni", Font.BOLD,30));
		button8.addActionListener(this); 
		button13.addActionListener(this);

		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 5;
		background.add(button8, OptionScreenFromGameScreenContraint);

		OptionScreenFromGameScreenContraint.gridx = 0;
		OptionScreenFromGameScreenContraint.gridy = 6;
		background.add(button13, OptionScreenFromGameScreenContraint);


		button8.setActionCommand("Main Menu");
		button13.setActionCommand("Back to Game");

		this.add(background);

	}
	@Override
	public void itemStateChanged(ItemEvent e) {


		if (track1toggle.isSelected()){
			try {
				Sounds.playClip1();
			} catch (UnsupportedAudioFileException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (LineUnavailableException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else {	

			Sounds.stopClip1();

		}	
		 if (track2toggle.isSelected()){
				try {
					Sounds.playClip2();
				} catch (UnsupportedAudioFileException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (LineUnavailableException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		}
		else {	
				Sounds.stopClip2();
		}



	}
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Main Menu"))){
			
			Game.exitOptions();
			
			Screens.goToMainMenu();

			state.welcomeScreenEntered(true);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
			
			Sounds.stopClip1();
			try {
				Sounds.playClip2();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}				
		else if ((name.getActionCommand().equals("Back to Game"))){
			
			Screens.goToGameScreenAlreadyStarted();
			
			if (state.statusOfOnePlayerGameScreen()==true){
				

				state.welcomeScreenEntered(false);
				state.onePlayerGameScreenEntered(true);
				state.twoPlayerGameScreenEntered(false);
				state.howManyPlayersScreenEntered(false);
				state.enterNameScreenEntered(false);
				state.highScoresScreenEntered(false);
				state.helpScreenEntered(false);
				state.optionScreenEntered(false);
				state.noHighScoreExitScreenEntered(false);
				state.highScoreExitScreenEntered(false);
			}
			else if (state.statusOfTwoPlayerGameScreen()==true){


				state.welcomeScreenEntered(false);
				state.onePlayerGameScreenEntered(false);
				state.twoPlayerGameScreenEntered(true);
				state.howManyPlayersScreenEntered(false);
				state.enterNameScreenEntered(false);
				state.highScoresScreenEntered(false);
				state.helpScreenEntered(false);
				state.optionScreenEntered(false);
				state.noHighScoreExitScreenEntered(false);
				state.highScoreExitScreenEntered(false);
			}
		}

	}				
}


