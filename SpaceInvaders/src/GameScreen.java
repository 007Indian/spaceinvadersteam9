import javax.swing.*; //importing the  entire Swing library

import java.awt.*; //importing the entire awt library to create user interface
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//creating the gameScreen frame
public class GameScreen extends JPanel implements ActionListener{

	GameControl state = new GameControl();
	
	// initialisations
	private static final long serialVersionUID = 1L;
	private int points = 0;
	private int playerLives = 0;
	private String message;
	private int counter = 0;
	private boolean multiplayer = false;
	private int playerLevel;
	private boolean priority = false;

	static boolean multiplayerMode;
	static boolean enable = false;

	//timer that invokes the action listener every 100ms
	Timer textTimer = new Timer(200, this);

	Game spaceInvaders; //spaceInvaders is type Game
	JLabel scoreNumber = new JLabel(); //scoreNumber is an object of type JLabel
	JLabel livesNumber = new JLabel(); //livesNumber is an object of type JLabel
	JLabel statusMessage = new JLabel(); //statusMessage is an object of type JLabel

	//GameScreen class constructor
	public GameScreen(boolean multi) {
		multiplayer = multi;
		setUpGameScreen(multiplayer); //method that sets up GameScreen
		textTimer.start(); //starting timer
	}

	//method that sets up GameScreen
	public void setUpGameScreen(boolean multiplayer) {

		spaceInvaders = new Game(multiplayer); //creating the Game class

//		Color moccasin = new Color(255, 239, 213); //pastel light yellow
		Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
		Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue


		this.setBackground(lighterBlue);
		this.setPreferredSize(new Dimension(800, 600));
		this.setLayout(new BorderLayout()); //gameScreen JFrame will have a BorderLayout

		//JPanel #1
		JPanel gameInfoPanel = new JPanel(); //new JPanel called gameInfoPanel
		gameInfoPanel.setLayout(new BorderLayout()); //using BorderLayout
		gameInfoPanel.setBackground(sandyBrown);

		//JPanel #2
		JPanel scorePanel = new JPanel(); //new JPanel called scorePanel
		scorePanel.setLayout(new FlowLayout(0, 3, 2)); //using FlowLayout with 3 pixels between components
		scorePanel.setBackground(sandyBrown);

		JLabel score = new JLabel("Score: ");
		score.setFont(new Font("Aharoni", Font.BOLD, 20));
		scorePanel.add(score); //inserting score label to scoreInfo panel
		score.setForeground(Color.BLACK);

		//scoreNumber will print the number of points the Player has
		scoreNumber.setText(String.valueOf(points)); //String.valueOf(points) converts int to string
		//System.out.println("JLabel points: " + scoreNumber);
		scoreNumber.setFont(new Font("Aharoni", Font.BOLD, 20));
		scorePanel.add(scoreNumber); //inserting score number label to scoreInfo panel
		scoreNumber.setForeground(Color.BLACK);

		//JPanel #3
		JPanel livesPanel = new JPanel(); //new JPanel called livesPanel
		livesPanel.setLayout(new FlowLayout(0, 3, 2)); //using FlowLayout with 3 pixels between components
		livesPanel.setBackground(sandyBrown);

		JLabel lives = new JLabel("Lives: ");
		lives.setFont(new Font("Aharoni", Font.BOLD, 20));
		livesPanel.add(lives); //inserting lives label to scoreInfo panel
		lives.setForeground(Color.BLACK);

		//livesNumber will print the number of points the Player has
		livesNumber.setText(String.valueOf(playerLives)); //String.valueOf(points) converts int to string
		livesNumber.setFont(new Font("Aharoni", Font.BOLD, 20));
		livesPanel.add(livesNumber); //inserting lives number label to scoreInfo panel
		livesNumber.setForeground(Color.BLACK);  

		//JPanel #4
		JPanel statusPanel = new JPanel(); //new JPanel called statusPanel
		livesPanel.setLayout(new FlowLayout(0, 3, 2)); //using FlowLayout with 3 pixels between components
		statusPanel.setBackground(sandyBrown);
		//		statusPanel.setBackground(Color.BLUE);

		//statusMessage will print any messages to the Player
		statusMessage.setText(message); //String.valueOf(points) converts int to string
		statusMessage.setFont(new Font("Aharoni", Font.BOLD, 20));
		statusPanel.add(statusMessage); //inserting statusMessage label to status panel
		statusPanel.setForeground(Color.BLACK);  

		//adding all the JPanels to GameInfoPanel JPanel
		gameInfoPanel.add(scorePanel, BorderLayout.WEST); //adding scorePanel to GameInfoPanel JPanel
		gameInfoPanel.add(livesPanel, BorderLayout.EAST); //adding livesPanel to GameInfoPanel JPanel
		gameInfoPanel.add(statusPanel, BorderLayout.CENTER); //adding livesPanel to GameInfoPanel JPanel
		this.add(gameInfoPanel, BorderLayout.PAGE_START); //adding gameInfoPanel to gameScreen JFrame
		this.add(spaceInvaders); //adding spaceInvaders to gameScreen JPanel

		spaceInvaders.requestFocus();
	}

	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) {

		//checking for enable signal
		if (enable == true) {
			if (spaceInvaders.hasGameBeenCompletelyInitialised == false) {
				//multiplayer mode selected so ensuring that both players are initialised properly
				if (multiplayerMode == true) {
					//player 1
					spaceInvaders.player1.setX(650);
					spaceInvaders.player1.setY(480);
					spaceInvaders.player1.setVisibility(true);

					//player 2
					spaceInvaders.player2.setX(100);
					spaceInvaders.player2.setY(480);
					spaceInvaders.player2.setVisibility(true);

					//single player mode selected so ensuring that only player1 is initialised properly, and the other is invisible
				} else if (multiplayerMode == false) {

					//player 1
					spaceInvaders.player1.setX(375);
					spaceInvaders.player1.setY(480);
					spaceInvaders.player1.setVisibility(true);

					if (spaceInvaders.playerHasPlayedMultiplayerBefore == true) {
						//player 2
						spaceInvaders.player2.setX(0);
						spaceInvaders.player2.setY(480);
						spaceInvaders.player2.setVisibility(false);
					}	
				}
				spaceInvaders.hasGameBeenCompletelyInitialised = true;
			}
			enable = false; //setting enable signal back to false
		}

		//retrieving the player's points and then printing it
		points = spaceInvaders.playership1.getPoints();
		scoreNumber.setText(String.valueOf(points));

		//retrieving the player's lives and then printing it
		playerLives = spaceInvaders.playership1.getLives();
		livesNumber.setText(String.valueOf(playerLives));

		//forming statuses that will be printed between score and lives
		if (priority == true) {
			//has high priority as player would have just finished a level so this needs to be printed
			if (counter < 2) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 6) {
				//congratulating player on finishing a level
				if (playerLevel > 1) {
					message = "Congratulations on finishing Level " + (playerLevel - 1);
					statusMessage.setText(message);
				}
				counter++;

			} else if (counter < 8) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 12) {
				//congratulating player on finishing a level
				if (playerLevel > 1) {
					message = "Congratulations on finishing Level " + (playerLevel - 1);
					statusMessage.setText(message);
				}
				counter++;

			} else if (counter < 14) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 18) {
				//informing player what the next level is
				message = "Level " + (playerLevel) + " is next";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 20) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 24) {
				//informing player what the next level is
				message = "Level " + (playerLevel) + " is next";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 26) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 30) {
				//informing player to press 'p' to start the level
				message = "Press 'p' to start";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 32) {
				message = " ";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 36) {
				//informing player to press 'p' to start the level
				message = "Press 'p' to start";
				statusMessage.setText(message);
				counter++;

			} else if (counter < 38) {
				priority = false;
				counter = 0;
			}

			if (spaceInvaders.getLevelStatus() == false) {
				priority = false;
			}


		//printing low priority statuses
		} else if (priority == false) {
			
			if (spaceInvaders.getLevelStatus() == true) {
				priority = true; //setting priority
				counter = 0;
				playerLevel = spaceInvaders.getPlayerLevel();
			}

			//checking if the game is paused
			if (spaceInvaders.getGamePauseState() == true) { //game is paused
				if (counter < 2) {
					message = " ";
					statusMessage.setText(message);
					counter++;

				} else if (counter < 6) {
					//informing the player the game is paused or how to start the game, if they have just started the game.
					if (spaceInvaders.hasGameStarted() == false) {
						if (multiplayer == true) {
							message = "Press the up arrow key or the space bar to start";
						}
						else if (multiplayer == false) {
							message = "Press the up arrow key to start";
						}
					}	else {
						message = "Game Paused";
					}
					statusMessage.setText(message);	
					counter++;
					
				} else if (counter < 8) {
					message = " ";
					statusMessage.setText(message);
					counter++;

				} else if (counter < 12) {
					//informing the player the game is paused or how to start the game, if they have just started the game.
					if (spaceInvaders.hasGameStarted() == false) {
						if (multiplayer == true) {
							message = "Press the up arrow key or the space bar to start";
						}
						else if (multiplayer == false) {
							message = "Press the up arrow key to start";
						}
					}	else {
						message = "Press 'p' to resume";
					}
					statusMessage.setText(message);	
					counter++;

				} else if (counter < 14) {
					message = " ";
					statusMessage.setText(message);
					counter = 0;

				}

			} else if (spaceInvaders.collected == true) {
				if (counter < 2) {
					message = " ";
					statusMessage.setText(message);
					counter++;
					// information about the power-up just collected by the player is displayed
				} else if (counter < 6 || ((counter > 7) && (counter < 12))) {
					if (spaceInvaders.tempNumber == 0) {
						message = "Enemy's firing increased!";
					}
					else if (spaceInvaders.tempNumber == 1) {
						message = "Enemy's firing decreased!";
					}
					else if (spaceInvaders.tempNumber == 2) {
						message = "One extra life";
					}
					else if (spaceInvaders.tempNumber == 3) {
						message = "Lost one life";
					}
					else if (spaceInvaders.tempNumber == 4) {
						message = "Player's speed increased!";
					}
					else if (spaceInvaders.tempNumber == 5) {
						message = "Player's speed decreased!";
					}
					else if (spaceInvaders.tempNumber == 6) {
						message = "Super Spaceship's appears more often!";
					}
					statusMessage.setText(message);
					counter++;
					
				} else if (counter < 8) {
					message = " ";
					statusMessage.setText(message);
					counter++;
					
				} else if (counter == 12) {
					message = " ";
					statusMessage.setText(message);
					counter = 0;
					spaceInvaders.collected = false;
				}

			} else {
				message = " ";
				statusMessage.setText(message);
				counter = 0;
			}
		}
	}
}