import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.table.*;

import java.util.*;

public class HighScoresScreen extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	JButton MainMenuButton;

	GameControl state = new GameControl();

	public HighScoresScreen() throws Exception{
		this.setPreferredSize(new Dimension(800, 600));
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));

		this.setBackground(navyBlue);

		JPanel Label = new JPanel();
		Label.setBackground(navyBlue);
		JPanel Label1 = new JPanel();
		Label1.setBackground(navyBlue);

		JLabel highScoreLabel2 = new JLabel("<html>  High Scores <html>", JLabel.CENTER);
		highScoreLabel2.setFont(new Font("Aharoni", Font.BOLD, 70));
		highScoreLabel2.setForeground(Color.WHITE);

		//Call the enter ReadLogFile class 
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Integer> scores = new ArrayList<Integer>();



		ReadLogFile read = new ReadLogFile();

		//assigns what is returned from the ReadLogFile class to the dynamic array list.
		names=read.returnNamesFromLogFile();
		scores=read.returnScoresFromLogFile();

		//Inputing values into the JTable only looking at the top 10 
		Object rowData[][] = { 	{ "1", names.get(names.size()-1), scores.get(scores.size()-1)},
				{ "2",names.get(names.size()-2), scores.get(scores.size()-2)},
				{ "3",  names.get(names.size()-3), scores.get(scores.size()-3)},
				{ "4", names.get(names.size()-4), scores.get(scores.size()-4)},
				{"5", names.get(names.size()-5), scores.get(scores.size()-5)},
				{"6", names.get(names.size()-6), scores.get(scores.size()-6)},
				{"7", names.get(names.size()-7), scores.get(scores.size()-7)},
				{"8", names.get(names.size()-8), scores.get(scores.size()-8)},
				{"9", names.get(names.size()-9), scores.get(scores.size()-9)},
				{"10", names.get(names.size()-10), scores.get(scores.size()-10)}
		};

		Object columnNames[] = { "Rank", "Name", "Score"};
		JTable jTable = new JTable(rowData, columnNames){
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int rowData, int columnNames){
				return false;
			}

			public Component prepareRenderer (TableCellRenderer r, int rowData, int columnNames){
				Component c = super.prepareRenderer(r,rowData,columnNames);
				//setting all the odd rows colour to white and the even to a shade of pastel blue
				if (rowData % 2 == 0){
					c.setBackground(Color.WHITE);
				}
				else{

					c.setBackground(babyBlue);
				}
				if (isCellSelected(rowData,columnNames)){

					c.setBackground(navyBlue);
				}

				return c;
			}
		};

		//setting the height and width of all the columns in the JTable
		jTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		jTable.getColumnModel().getColumn(0).setPreferredWidth(125);
		jTable.getColumnModel().getColumn(1).setPreferredWidth(431);
		jTable.getColumnModel().getColumn(2).setPreferredWidth(225);
		jTable.setRowHeight(jTable.getRowHeight() + 30);
		jTable.setFont(new Font("Helvetica Neue Thin", Font.PLAIN, 24));
		JTableHeader header = jTable.getTableHeader();
		header.setPreferredSize(new Dimension(100, 50));
		header.setFont(new Font("Helvetica Neue Thin", Font.PLAIN, 30));

		//creating a scroll pane for the JTable so that many scores can be displayed on the same table
		JScrollPane jps = new JScrollPane(jTable);

		Label1.add(highScoreLabel2);

		this.add(Label1);
		this.add(jps);


		MainMenuButton = new JButton("Main Menu");
		MainMenuButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		MainMenuButton.addActionListener(this);
		MainMenuButton.setForeground(lighterBlue);
		this.add(MainMenuButton);


	}

	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Main Menu"))){
			Screens.goToMainMenu();

			state.welcomeScreenEntered(true);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);


		}
	}

}
