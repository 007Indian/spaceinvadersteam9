import java.util.concurrent.atomic.AtomicBoolean;

abstract class PlayerThread {

	private AtomicBoolean running; // thread-safe boolean
	private Thread gameThread; // thread name

	public PlayerThread() {

		gameThread = null; //initial status
		running = new AtomicBoolean(false); // thread not running to start off with

	}

	//starting thread
	void start() {
		if (gameThread != null) { // check if thread is something other than null, i.e running or stopped.
			if (gameThread.isAlive()) { //check to see if it's already running, and if it is do nothing.
				return;
			}
		}

		gameThread = new Thread(new Runnable() { // create a new thread
			@Override
			public void run() {
				playerLoop();
			}
		});

		gameThread.start(); // start the thread
		running.set(true); // set the thread as running
	}

	void pausing() {

		running.set(false); // pause the thread by stopping it, until started again
	}

	private void playerLoop() {

		while (running.get()) {
			try {
				Thread.sleep(5); //sleeping for 5ms to avoid too much CPU usage
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			update();//Update the entity movements and collision checks etc

		}
	}

	//thread updated after a certain time
	abstract void update();
}
