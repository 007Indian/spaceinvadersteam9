import java.io.*;
import javax.sound.sampled.*;

public class Sounds {
	
	static GameControl state = new GameControl();


	static Clip missileSounds;
	static Clip playerIsShot;
	static Clip powerUpCollected;
	static Clip backgroundSong1;
	static Clip backgroundSong2;

	//this method can be called to initiate the missile sounds method
	public static void missileSounds() throws UnsupportedAudioFileException, IOException, LineUnavailableException{

		AudioInputStream audio = AudioSystem.getAudioInputStream(new File("shot.wav"));
		AudioFormat form = audio.getFormat();
		DataLine.Info data = new DataLine.Info(Clip.class,form);
		missileSounds = (Clip) AudioSystem.getLine(data);
		missileSounds.open(audio);
		missileSounds.start();
	}
	//this missile is used when the player is shot or when the super space ship is shot
	public static void playerIsShot() throws UnsupportedAudioFileException, IOException, LineUnavailableException{

		AudioInputStream audio = AudioSystem.getAudioInputStream(new File("explosion_x.wav"));
		AudioFormat form = audio.getFormat();
		DataLine.Info data = new DataLine.Info(Clip.class,form);
		playerIsShot = (Clip) AudioSystem.getLine(data);
		playerIsShot.open(audio);
		playerIsShot.start();
	}
	//this is played when the power up package is collected
	public static void powerUpCollected() throws UnsupportedAudioFileException, IOException, LineUnavailableException{

		AudioInputStream audio = AudioSystem.getAudioInputStream(new File("Power-up Sounds.wav"));
		AudioFormat form = audio.getFormat();
		DataLine.Info data = new DataLine.Info(Clip.class,form);
		powerUpCollected = (Clip) AudioSystem.getLine(data);
		powerUpCollected.open(audio);
		powerUpCollected.start();
	}
	//this is used to play the song in game
	public static void backgroundSong1() throws UnsupportedAudioFileException, IOException, LineUnavailableException{

		AudioInputStream audio = AudioSystem.getAudioInputStream(new File("Bob's_Beat.wav"));
		AudioFormat form = audio.getFormat();
		DataLine.Info data = new DataLine.Info(Clip.class,form);
		backgroundSong1 = (Clip) AudioSystem.getLine(data);
		backgroundSong1.open(audio);
		backgroundSong1.start();
		backgroundSong1.addLineListener(new LineListener(){
			@Override
			public void update(LineEvent event) {
					backgroundSong1.loop(Clip.LOOP_CONTINUOUSLY);
			}

		});
	}
	//this can be called to stop the music while in game
	public static void stopClip1(){
		if (backgroundSong1.isRunning()){
			backgroundSong1.close();
		}
	}
	//plays the song while in game
	public static void playClip1() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

		if (!backgroundSong1.isRunning()){
			Sounds.backgroundSong1();
		}

	}	
	//this is song is heard in the welcome screens
	public static void backgroundSong2() throws UnsupportedAudioFileException, IOException, LineUnavailableException{

		AudioInputStream audio = AudioSystem.getAudioInputStream(new File("Space Invaders Soundtrack.wav"));
		AudioFormat form = audio.getFormat();
		DataLine.Info data = new DataLine.Info(Clip.class,form);
		backgroundSong2 = (Clip) AudioSystem.getLine(data);
		backgroundSong2.open(audio);
		backgroundSong2.start();
		backgroundSong2.addLineListener(new LineListener(){
			@Override
			public void update(LineEvent event) {
					backgroundSong2.loop(Clip.LOOP_CONTINUOUSLY);
			}

		});
	}
	//methods that stops clip2  
	public static void stopClip2(){
		if (backgroundSong2.isRunning()){
			backgroundSong2.close();
		}
	}
	
	//plays clip2
	public static void playClip2() throws UnsupportedAudioFileException, IOException, LineUnavailableException {

		if (backgroundSong2.isRunning()==false){
			Sounds.backgroundSong2();
		}

	}	
}
