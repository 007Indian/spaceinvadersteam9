import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class EnemyInformationScreen extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();

	public EnemyInformationScreen(){
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints helpScreenContraint = new GridBagConstraints();
		helpScreenContraint.insets = new Insets(0,0,0,0);
		
		//CREATING THE COMPONENTS NEEDING TO BE ADDED TO THE BACKGROUND
		JLabel helpLabel1 = new JLabel("<html> ENEMY INFORMATION <html>", JLabel.CENTER);

		helpLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel1.setForeground(Color.WHITE);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 0;

		background.add(helpLabel1,helpScreenContraint);

		JLabel helpLabel2 = new JLabel("<html><br>If an enemy in the first row is killed the player is rewarded 10 points.");
		JLabel helpLabel3 = new JLabel("<html>-An enemy in the first row can be killed with one shot. <br><br>");
		
		JLabel helpLabel4 = new JLabel("<html>If an enemy in second or third row is killed the player is rewarded 20 points.");
		JLabel helpLabel5 = new JLabel("<html>-An enemy in the second row requires two shots to be killed.<br><br>");
		
		JLabel helpLabel6 = new JLabel("<html>-An enemy in the third row requires three shots to be killed spaceship.<br><br> ");
		
		JLabel helpLabel7 = new JLabel("<html>If an enemy in fourth row is killed the player is rewarded 10 points.");
		JLabel helpLabel8 = new JLabel("<html>-The fourth row requires four shots to be killed from the player spaceship.<br><br> ");
		
		JLabel helpLabel9 = new JLabel("<html>If the super spaceships is killed the player is rewarded 100 points.");
		JLabel helpLabel10 = new JLabel("<html>-The super spaceship drops a power-up package ");
		JLabel helpLabel11 = new JLabel("<html>which if collected can be beneficial or detrimental.<br><br>");
		
		//These constraints define where each component will be placed
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 2;

		helpLabel2.setForeground(Color.GREEN);
		helpLabel2.setFont(new Font("Arial", Font.PLAIN, 20));

		background.add(helpLabel2,helpScreenContraint);	

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 3;

		helpLabel3.setForeground(Color.WHITE);
		helpLabel3.setFont(new Font("Arial", Font.PLAIN, 20));

		background.add(helpLabel3,helpScreenContraint);	

		helpLabel4.setForeground(Color.ORANGE);
		helpLabel4.setFont(new Font("Arial", Font.PLAIN, 22));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 4;

		background.add(helpLabel4,helpScreenContraint);	

		helpLabel5.setForeground(Color.WHITE);
		helpLabel5.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 5;

		background.add(helpLabel5,helpScreenContraint);	

		helpLabel6.setForeground(Color.WHITE);
		helpLabel6.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 6;

		background.add(helpLabel6,helpScreenContraint);

		helpLabel7.setForeground(Color.RED);
		helpLabel7.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 7;

		background.add(helpLabel7,helpScreenContraint);	

		helpLabel8.setForeground(Color.WHITE);
		helpLabel8.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 8;

		background.add(helpLabel8,helpScreenContraint);	

		helpLabel9.setForeground(Color.PINK);
		helpLabel9.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 9;

		background.add(helpLabel9,helpScreenContraint);	

		helpLabel10.setForeground(Color.WHITE);
		helpLabel10.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 10;

		background.add(helpLabel10,helpScreenContraint);	

		helpLabel11.setForeground(Color.WHITE);
		helpLabel11.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 11;

		background.add(helpLabel11,helpScreenContraint);	

		JButton button5 = new JButton("Back");
		button5.setFont(new Font("Aharoni", Font.BOLD, 30));
		button5.addActionListener(this); 
		button5.setForeground(navyBlue);
		button5.setPreferredSize(new Dimension(175, 50));
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 16;
		background.add(button5,helpScreenContraint);
		button5.setActionCommand("Back");

		this.add(background);
		
		//Validating a container means laying out its subcomponents. 
		//Layout-related changes, such as setting the bounds of a component, or adding a component to the container
		validate();
	}

	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Back"))){
			Screens.goToHelpScreenFromWelcomeScreen();

		}
	}
}




