import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

public class Screens extends JFrame implements ActionListener, KeyListener
{
	private static final long serialVersionUID = 1L;

	static CardLayout card;
	JButton startGameButton, helpButton, optionsButton, exitButton, highScoresButton;

	private static boolean firstGame = true; //if true, it means player is playing their first game

	static JPanel cards;

	//pastel colour scheme for buttons
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue


	static GameControl state = new GameControl();

	public Screens() throws Exception{

		//initiating sounds
		Sounds.backgroundSong1();

		//stopping one sound
		Sounds.stopClip1();


		Sounds.backgroundSong2();



		//No single button can be repeated in two panels eg. the main menu button is in the Option menu and High Scores and requires 
		//unique main menu buttons hence button 9 and button 10 have the same name.
		startGameButton = new JButton("Start Game");
		helpButton = new JButton("Help");
		optionsButton = new JButton("Options");
		exitButton = new JButton("Exit");
		highScoresButton = new JButton("High Scores");


		//setting the font, size and type of font to each JButton
		startGameButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		helpButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		optionsButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		exitButton.setFont(new Font("Aharoni", Font.BOLD, 30));
		highScoresButton.setFont(new Font("Aharoni", Font.BOLD, 30));


		//'this' keyword in java refers to it's self
		startGameButton.addActionListener(this);
		helpButton.addActionListener(this);
		optionsButton.addActionListener(this);  
		exitButton.addActionListener(this); 
		highScoresButton.addActionListener(this); 

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////          
		//adding the image as a background to the main screen

		//creating the main menu welcome screen   
		JPanel welcomeCard = new JPanel();

		welcomeCard.addKeyListener(this);

		WelcomeScreen welcome = new WelcomeScreen();

		JLabel background=new JLabel(new ImageIcon("Stars.png"));

		welcome.add(background);

		//setting the layout type for background
		background.setLayout(new GridBagLayout());

		GridBagConstraints welcomeScreenContraint = new GridBagConstraints();

		//this code can change the spacing indentation of the components on the layout
		welcomeScreenContraint.insets = new Insets(0,0,20,0);

		//ADDING ALL THE COMPONENTS ON THE MAIN MENU SCREEN
		JLabel welcomeLabel = new JLabel("<html>Space Invaders<br><br><html>");
		welcomeLabel.setFont(new Font("Aharoni", Font.BOLD, 70));
		welcomeLabel.setForeground(Color.WHITE);

		background.add(welcomeLabel);

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 1;

		startGameButton.setPreferredSize(new Dimension(250, 50));
		background.add(startGameButton,welcomeScreenContraint);

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 2;

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 3;

		highScoresButton.setPreferredSize(new Dimension(250, 50));
		background.add(highScoresButton,welcomeScreenContraint);

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 4;

		helpButton.setPreferredSize(new Dimension(250, 50));
		background.add(helpButton,welcomeScreenContraint);

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 5;

		optionsButton.setPreferredSize(new Dimension(250, 50));
		background.add(optionsButton,welcomeScreenContraint);

		welcomeScreenContraint.gridx = 0;
		welcomeScreenContraint.gridy = 6;

		exitButton.setPreferredSize(new Dimension(250, 50));
		background.add(exitButton,welcomeScreenContraint);   

		validate();

		//creating the enterName screen
		EnterNameScreen enterName = new EnterNameScreen();

		//creating the help screen
		GameNavigationScreen gameNavigationScreen = new GameNavigationScreen();
		//creating the power up help screen from game screen
		HelpScreenFromGameScreen helpScreenFromGameScreen = new HelpScreenFromGameScreen();
		//creating the power up help screen from welcome screen
		PowerUpScreenFromWelcomeScreen powerUpsScreenFromWelcomeScreen = new PowerUpScreenFromWelcomeScreen();
		//creating the enemy information help screen
		EnemyInformationScreen enemyInformationScreen = new EnemyInformationScreen();
		//A help screen to display the controls
		ControlsHelpScreen controlsHelpScreen = new ControlsHelpScreen();
		//creating the help screen
		HelpScreenFromWelcomeScreen helpFromWelcomeScreen = new HelpScreenFromWelcomeScreen();
		//creating the High Score screen
		HighScoresScreen highScores = new HighScoresScreen();
		//creating the option menu from the game screen
		OptionScreenFromGameScreen option = new OptionScreenFromGameScreen();
		//creating the option menu from the welcome screen
		OptionScreenFromWelcomeScreen optionsFromWelcomeScreen = new OptionScreenFromWelcomeScreen();

		/*creating the game screen
		gets created when you press the button*/

		//constructor to invoke the class howManyPlayers Screen
		HowManyPlayersScreen howManyPlayers = new HowManyPlayersScreen();
		//creating the no high score exit screen menu
		NoHighScoreExitScreen noHighScoreExit = new NoHighScoreExitScreen();
		//creating the high score exit screen menu
		HighScoreExitScreen highScoreExit = new HighScoreExitScreen();

		//Create the panel that contains the "cards".
		card = new CardLayout();
		card.addLayoutComponent(welcome, "WelcomeScreen");
		card.addLayoutComponent(enterName, "EnterNameScreen");
		card.addLayoutComponent(gameNavigationScreen,"GameNavigationScreen");
		card.addLayoutComponent(helpFromWelcomeScreen,"HelpScreenFromWelcomeScreen");
		card.addLayoutComponent(helpScreenFromGameScreen, "HelpScreenFromGameScreen");
		card.addLayoutComponent(controlsHelpScreen, "ControlsHelpScreen");
		card.addLayoutComponent(powerUpsScreenFromWelcomeScreen, "PowerUpScreenFromWelcomeScreen");
		card.addLayoutComponent(enemyInformationScreen, "EnemyInformationScreen");
		card.addLayoutComponent(highScores, "HighScoresScreen");
		card.addLayoutComponent(option, "OptionScreenFromGameScreen");
		card.addLayoutComponent(optionsFromWelcomeScreen, "OptionScreenFromWelcomeScreen");
		card.addLayoutComponent(howManyPlayers, "HowManyPlayersScreen");
		card.addLayoutComponent(noHighScoreExit, "NoHighScoreExitScreen");
		card.addLayoutComponent(highScoreExit, "HighScoreExitScreen");

		//adding the card type to a panel that can be invoked to be switch between
		cards= new JPanel(card);
		cards.add(welcome);
		cards.add(enterName);
		cards.add(highScores);
		cards.add(gameNavigationScreen);
		cards.add(helpScreenFromGameScreen);
		cards.add(controlsHelpScreen);
		cards.add(powerUpsScreenFromWelcomeScreen);
		cards.add(enemyInformationScreen);
		cards.add(helpFromWelcomeScreen);
		cards.add(option);
		cards.add(optionsFromWelcomeScreen);
		cards.add(howManyPlayers);
		cards.add(noHighScoreExit);
		cards.add(highScoreExit);

		getContentPane().add(cards); 

	}
	//if the button name is equal to that specified below the card will change to it's respective card
	@Override
	public void actionPerformed(ActionEvent name) {

		if ((name.getActionCommand().equals("Help"))){
			//Changing the panel to the Help Screen
			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "HelpScreenFromWelcomeScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(true);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
		}

		//close the screen and ensures it is not running in the background
		else if ((name.getActionCommand().equals("Exit"))){
			System.exit(0);
		}
		else if ((name.getActionCommand().equals("Options"))){
			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "OptionScreenFromWelcomeScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(true);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
		}
		else if ((name.getActionCommand().equals("Start Game"))){

			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "HelpScreenFromGameScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(true);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
		}
		else if ((name.getActionCommand().equals("High Scores"))){
			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "HighScoresScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(true);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {


	}

	// if the key specified below is pressed it will invoke the code required
	@Override
	public void keyPressed(KeyEvent e) {

		//uses the escape key to terminate the program, it has first precedence over any other keyListener
		if (e.getKeyCode()==KeyEvent.VK_ESCAPE){

			System.exit(0);
		}
		//shortcut to go the no high score exit screen
		else if ((e.getKeyCode()==KeyEvent.VK_PAGE_DOWN)){
			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "NoHighScoreExitScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(true);
			state.highScoreExitScreenEntered(false);
		}

		//shortcut to go the high score exit screen
		else if ((e.getKeyCode()==KeyEvent.VK_PAGE_UP)){
			CardLayout cardLayout = (CardLayout) cards.getLayout();
			cardLayout.show(cards, "HighScoreExitScreen");

			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(true);
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {


	}

	//these methods can called from different classes to change the panel that is viewed
	public static void changeToNoHighScoreExitScreen() {

		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "NoHighScoreExitScreen");


		state.welcomeScreenEntered(false);
		state.onePlayerGameScreenEntered(false);
		state.twoPlayerGameScreenEntered(false);
		state.howManyPlayersScreenEntered(false);
		state.enterNameScreenEntered(false);
		state.highScoresScreenEntered(false);
		state.helpScreenEntered(false);
		state.optionScreenEntered(false);
		state.noHighScoreExitScreenEntered(true);
		state.highScoreExitScreenEntered(false);


	}
	public static void changeToHighScoreExitScreen() {

		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "HighScoreExitScreen");

		state.welcomeScreenEntered(false);
		state.onePlayerGameScreenEntered(false);
		state.twoPlayerGameScreenEntered(false);
		state.howManyPlayersScreenEntered(false);
		state.enterNameScreenEntered(false);
		state.highScoresScreenEntered(false);
		state.helpScreenEntered(false);
		state.optionScreenEntered(false);
		state.noHighScoreExitScreenEntered(false);
		state.highScoreExitScreenEntered(true);


	}
	public static void goToMainMenu() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "WelcomeScreen");
	}
	public static void goToOptionScreenFromGameScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "OptionScreenFromGameScreen");		
	}
	
	//this function initiates the single player game screen
	public static void goToSinglePlayerGameScreen() {
		if (firstGame == true) {
			GameScreen game = new GameScreen(false);			
			card.addLayoutComponent(game, "GameScreen");
			cards.add(game);

			firstGame = false;
		}

		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "GameScreen");

		GameScreen.multiplayerMode = false;
		GameScreen.enable = true;

		System.out.println("Screens, GameScreen.multiplayerMode = false;");

		System.out.println("GameScreen.multiplayerMode = false;");

		Sounds.stopClip2();
		try {
			Sounds.playClip1();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}


	}
	///this funtion initiates the two player game screen
	public static void goToTwoPlayerGameScreen() {
		if (firstGame == true) {
			GameScreen game = new GameScreen(true);
			card.addLayoutComponent(game, "GameScreen");
			cards.add(game);

			firstGame = false;
		}

		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "GameScreen");

		GameScreen.multiplayerMode = true;
		GameScreen.enable = true;


		System.out.println("GameScreen.multiplayerMode = true;");

		Sounds.stopClip2();
		try {
			Sounds.playClip1();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	//this is used when options is pressed inside the game screen
	public static void goToGameScreenAlreadyStarted() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "GameScreen");
	}
	//THE FUNCTION BELOW GO TO THE SCREENS RESPECTIVE THE THE METHOD NAMES 
	public static void goToEnterNameScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "EnterNameScreen");		
	}
	public static void goToHelpFromWelcomeScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "HelpFromWelcomeScreen");
	}
	public static void goToHowManyPlayersScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "HowManyPlayersScreen");
	}
	public static void goToHelpScreenFromWelcomeScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "HelpScreenFromWelcomeScreen");

	}
	public static void goToPowerUpScreenFromWelcomeScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "PowerUpScreenFromWelcomeScreen");	
	}
	public static void goToEnemyInformationScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "EnemyInformationScreen");
	}
	public static void goToGameNavigationScreen() {
		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "GameNavigationScreen");

	}
	public static void goToControlsHelpScreen() {

		CardLayout cardLayout = (CardLayout) cards.getLayout();
		cardLayout.show(cards, "ControlsHelpScreen");
	}

}


