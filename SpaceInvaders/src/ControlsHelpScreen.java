import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class ControlsHelpScreen extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();

	public ControlsHelpScreen(){
		
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints helpScreenContraint = new GridBagConstraints();
		helpScreenContraint.insets = new Insets(0,0,0,0);

		//CREATING ALL THE COMPONENTS NEEDING TO BE PUT INTO THE GAME
		JLabel helpLabel1 = new JLabel("<html> CONTROLS <br> <html>", JLabel.CENTER);

		helpLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel1.setForeground(Color.WHITE);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 0;

		background.add(helpLabel1,helpScreenContraint);
		
		JLabel helpLabel2 = new JLabel("<html><br><br>Single Player Controls");
		JLabel helpLabel3 = new JLabel("<html>Press the 'left' and 'right' arrow keys to move left and right respectively");
		JLabel helpLabel4 = new JLabel("Press the 'up' arrow key to fire");
		JLabel helpLabel5 = new JLabel("Multiplayer Controls");
		JLabel helpLabel6 = new JLabel("Player 1");
		JLabel helpLabel7 = new JLabel("<html>Press the 'left' and 'right' arrow keys to move left and right respectively");
		JLabel helpLabel8 = new JLabel("Press the 'up' arrow key to fire");
		JLabel helpLabel9 = new JLabel("Player 2");
		JLabel helpLabel10 = new JLabel("Press the 'z' key to move left and the 'x'key to move right");
		JLabel helpLabel11 = new JLabel("<html>Press 'space bar'to fire<br><br><br><br>");
		
		//ADDING ALL THE COMPONENTS NEEDING TO BE PUT INTO THE GAME
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 2;

		helpLabel2.setForeground(Color.RED);
		helpLabel2.setFont(new Font("Arial", Font.BOLD, 20));

		background.add(helpLabel2,helpScreenContraint);	

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 3;

		helpLabel3.setForeground(Color.WHITE);
		helpLabel3.setFont(new Font("Arial", Font.PLAIN, 20));

		background.add(helpLabel3,helpScreenContraint);	

		helpLabel4.setForeground(Color.WHITE);
		helpLabel4.setFont(new Font("Arial", Font.PLAIN, 22));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 4;

		background.add(helpLabel4,helpScreenContraint);	

		helpLabel5.setForeground(Color.RED);
		helpLabel5.setFont(new Font("Arial", Font.BOLD, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 5;

		background.add(helpLabel5,helpScreenContraint);	

		helpLabel6.setForeground(Color.WHITE);
		helpLabel6.setFont(new Font("Arial", Font.BOLD, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 6;

		background.add(helpLabel6,helpScreenContraint);
		
		helpLabel7.setForeground(Color.WHITE);
		helpLabel7.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 7;

		background.add(helpLabel7,helpScreenContraint);
		
		helpLabel8.setForeground(Color.WHITE);
		helpLabel8.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 8;

		background.add(helpLabel7,helpScreenContraint);
		
		helpLabel9.setForeground(Color.WHITE);
		helpLabel9.setFont(new Font("Arial", Font.BOLD, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 9;

		background.add(helpLabel9,helpScreenContraint);
		
		helpLabel10.setForeground(Color.WHITE);
		helpLabel10.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 10;

		background.add(helpLabel10,helpScreenContraint);
		
		helpLabel11.setForeground(Color.WHITE);
		helpLabel11.setFont(new Font("Arial", Font.PLAIN, 20));

		//setting positions on screen for components
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 11;

		background.add(helpLabel11,helpScreenContraint);

		JButton button5 = new JButton("Back");
		button5.setFont(new Font("Aharoni", Font.BOLD, 30));
		button5.addActionListener(this); 
		button5.setForeground(navyBlue);
		button5.setPreferredSize(new Dimension(175, 50));
		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 16;
		background.add(button5,helpScreenContraint);
		button5.setActionCommand("Back");

		this.add(background);
		validate();
	}

	//when the back button is pressed it goes to the welcome screen
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Back"))){
			Screens.goToHelpScreenFromWelcomeScreen();

		}


	}
}