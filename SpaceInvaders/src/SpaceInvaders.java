import javax.swing.*;



public class SpaceInvaders{


	public static void main(String[] args) throws Exception {
		
		//initiates the screens class where the hierarchy begins
		Screens mainScreen = new Screens();
	
		mainScreen.setTitle("Space Invaders");
		//sets the size of the frame
		mainScreen.setSize(800,600);
		mainScreen.setFocusable(true);
		mainScreen.setVisible(true);
		mainScreen.setResizable(false);
		//close the screen and ensures it is not running in the background when the "x" button is pressed
		mainScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainScreen.addKeyListener(mainScreen);
		
	}


}

