import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class GameNavigationScreen extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();


	public GameNavigationScreen(){
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints helpScreenContraint = new GridBagConstraints();
		helpScreenContraint.insets = new Insets(0,0,10,0);

		JLabel helpLabel1 = new JLabel("<html> NAVIGATION HELP! <html>", JLabel.CENTER);

		helpLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		helpLabel1.setForeground(Color.WHITE);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 0;

		background.add(helpLabel1,helpScreenContraint);

		JLabel helpLabel4 = new JLabel("<html><br><br>'Start Game', initiates the game; 'Enter Name' enables the player to enter their name for");
		JLabel helpLabel5 = new JLabel("high score purposes, 'High scores' shows the top ten scores and the respective player's");
		JLabel helpLabel6 = new JLabel("name, 'Options' presents a menu where the player can make adjustments to the game,");
		JLabel helpLabel7 = new JLabel("such as the sound levels and keyboard controls; 'Help' takes the player through the");
		JLabel helpLabel8 = new JLabel("tutorial, menu where the player can make adjustments to the game, such as the sound");
		JLabel helpLabel9 = new JLabel("levels and keyboard controls; 'Help' takes the player through the tutorial again;");
		JLabel helpLabel10 = new JLabel("and 'Exit' closes the application. When the game is over, there will be an Exit");
		JLabel helpLabel11 = new JLabel("Screen which shows the score from that game, and whether it made it into the high");
		JLabel helpLabel12 = new JLabel("<html>score rankings. The Exit Screen will also give an option to exit to the main menu.<br><br>");

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 2;	

		helpLabel4.setForeground(Color.WHITE);
		helpLabel4.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 4;

		background.add(helpLabel4,helpScreenContraint);	

		helpLabel5.setForeground(Color.WHITE);
		helpLabel5.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 5;

		background.add(helpLabel5,helpScreenContraint);	

		helpLabel6.setForeground(Color.WHITE);
		helpLabel6.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 6;

		background.add(helpLabel6,helpScreenContraint);

		helpLabel7.setForeground(Color.WHITE);
		helpLabel7.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 7;

		background.add(helpLabel7,helpScreenContraint);	

		helpLabel8.setForeground(Color.WHITE);
		helpLabel8.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 8;

		background.add(helpLabel8,helpScreenContraint);	

		helpLabel9.setForeground(Color.WHITE);
		helpLabel9.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 9;

		background.add(helpLabel9,helpScreenContraint);	

		helpLabel10.setForeground(Color.WHITE);
		helpLabel10.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 10;

		background.add(helpLabel10,helpScreenContraint);	

		helpLabel11.setForeground(Color.WHITE);
		helpLabel11.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 11;

		background.add(helpLabel11,helpScreenContraint);	

		helpLabel12.setForeground(Color.WHITE);
		helpLabel12.setFont(new Font("Arial", Font.PLAIN, 20));

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 12;

		background.add(helpLabel12,helpScreenContraint);	

		JButton button5 = new JButton("Back");
		button5.setFont(new Font("Aharoni", Font.BOLD, 30));
		button5.addActionListener(this); 
		button5.setForeground(navyBlue);

		helpScreenContraint.gridx = 0;
		helpScreenContraint.gridy = 13;
		background.add(button5,helpScreenContraint);

		button5.setActionCommand("Back");

		this.add(background);
		validate();
	}

	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Back"))){
			
			Screens.goToHelpScreenFromWelcomeScreen();

		}
	}
}




