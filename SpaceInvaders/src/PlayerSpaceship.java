import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class PlayerSpaceship extends Object {

	private static Image PlayerSpaceshipImage;
	private static int totalPoints = 0; //variable that will hold the total number of points for the player
	private int numberOfLives; //variable that will hold the number of lives remaining for the player
	GameControl state = new GameControl();

	// initialisations
	protected Double rectangle;
	private boolean visible = true, left = false, right = false, shot = false;

	public PlayerSpaceship(Image player, int x, int y) {
		PlayerSpaceship.PlayerSpaceshipImage = player; // setting the playerspaceship as a image
		// setting rectangle to be set as a player image that will get redrawn in the next repaint
		rectangle = new Rectangle2D.Double(x, y, 50, 50);
	}

	//returning the image of the player
	public static Image getImage(){
		return PlayerSpaceshipImage;
	}
	
	//setting left, if it's true it means the player wants to move left
	public void setLeft(boolean leftStatus) {
		left = leftStatus;
	}
	
	//setting right, if it's true it means the player wants to move right
	public void setRight(boolean rightStatus) {
		right = rightStatus;
	}
	
	//setting shot, if it's true it means the player wants to move shoot and has shoot
	public void setShot(boolean shotStatus) {
		shot = shotStatus;
	}
	
	//if true it means the player has just shot in this button press
	public boolean getShot() {
		return shot;
	}
	
	//increasing the points for the player
	public void increasePoints(int points) {
		totalPoints += points;
	}

	//setting the points for the player
	public void setPoints(int points) {
		totalPoints = points;
	}

	//returning the points of the player
	public static int getPoints() {
		return totalPoints;
	}

	//setting the points for the player
	public void setLives(int lives) {
		numberOfLives = lives;
	}

	//increasing the number lives of player
	public void increaseLife(int i) {
		numberOfLives = numberOfLives + i;
	}

	//decreasing the number lives of player
	public void decreaseLife() {
		numberOfLives--;

		//playing sound effect
		try {
			Sounds.playerIsShot();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}

	}

	//returning the number of lives remaining for the player
	public int getLives() {
		return numberOfLives;
	}
	
	//returning the x position of the player
	public double getX() {
		return rectangle.x;
	}
	
	//returning the y position of the player
	public double getY() {
		return rectangle.y;
	}

	//setting the x position for the player
	public void setX(int xPosition) {
		rectangle.x = xPosition;
	}

	//setting the y position for the player
	public void setY(int yPosition) {
		rectangle.y = yPosition;
	}
	
	//returning the visibility of the player
	public boolean getVisibility() {
		return visible;
	}
	
	//setting the visibility of the player
	public void setVisibility(boolean visibility) {
		visible = visibility;
	}
	
	// used to move the player
	public void move(boolean power, boolean speed) {

		// logic for default movement speed(2), of the player, so when a power up has not been collected and i.e power == false.
		// left is true when the command for moving left is pressed
		// right is true when the command for moving right is pressed
		if ((power == false) && ((speed == true) || (speed == false))) {
			if ((left == true) && (rectangle.x - 5) >= 0) { // boundary limits so stops the player at the left wall
				rectangle.x -= 2; // move the player left by 2 pixels
			}

			if ((right == true) && (rectangle.x + 5) + 50 <= 800) { // boundary limits so stops the player at the right wall
				rectangle.x += 2; // move the player right by 2 pixels
			}
		}
		// logic for when a power up has been collected, i.e power == true and if it's a power up that speeds up the player i.e speed == true
		else if ((power == true) && (speed == true)) {
			if ((left == true) && (rectangle.x - 5) >= 0) { // boundary limits so stops the player at the left wall
				rectangle.x -= 3;  // move the player left by 3 pixels
			}

			if ((right == true) && (rectangle.x + 5) + 50 <= 800) { // boundary limits so stops the player at the right wall
				rectangle.x += 3;  // move the player right by 3 pixels
			}
		}
		// logic for when a power up has been collected, i.e power == true and if it's a power up that slows down the player i.e speed == false
		else if ((power == true) && (speed == false)) {
			if ((left == true) && (rectangle.x - 5) >= 0) { // boundary limits so stops the player at the left wall
				rectangle.x -= 1;  // move the player left by 1 pixels
			}

			if ((right == true) && (rectangle.x + 5) + 50 <= 800) { // boundary limits so stops the player at the right wall
				rectangle.x += 1;  // move the player right by 1 pixels
			}
		}
	}

}