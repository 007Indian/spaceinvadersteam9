import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;

public class Missiles extends Object {

	private static Image missilesImage; //missileImage is an object of type Image

	boolean bulletDirection; //direction of bullet, either upwards (ture) or downwards (false)
	protected Double bullets;
	private boolean visible;
	int bulletWidth = 5; //bullet's width
	int bulletHeight = 10; //bullet's height

	//constructor for the Missiles class
	public Missiles(Image missile, int xPosition, int yPosition) {
		Missiles.missilesImage = missile;
		visible = true; //making it visible

		//adding the bullet to the List of GameObjects that will be drawn on next screen paint
		bullets = new Rectangle2D.Double(xPosition, yPosition, bulletWidth, bulletHeight);

	}

	//returns the image of the missile
	public static Image getImage(){
		return missilesImage;
	}	

	//retrieves the x position of the bullet
	public double getX() {
		return bullets.x;
	}

	//retrieves the y position of the bullet
	public double getY() {
		return bullets.y;
	}

	//retrieves visibility of missile
	public boolean getVisibility() {
		return visible;
	}
	
	// the visibility of the missle
	public void setVisibility(boolean visibility) {
		visible = visibility;
	}

	//moves bullet
	public void move() {

		if ((bullets.y - 4) <= 0) { //removing the bullet 4 pixels from the top of the panel
			setVisibility(false);
		} else {
			bullets.y -= 4; //decrementing the bullet's position by 4 pixels
		}
	}

}
