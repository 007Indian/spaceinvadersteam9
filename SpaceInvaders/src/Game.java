import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

public class Game extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	//calling the game control class to be able to access it's methods from this file 
	GameControl state = new GameControl();

	public static PlayerThread playerLoop; //for threading
	
	private int width, height; //of the player and enemies
	public int tempNumber; // for power-ups

	//ArrayLists that holds enemies and missiles
	static ArrayList<EnemySpaceShip> noobEnemiesArray = new ArrayList<EnemySpaceShip>(); //noob enemies
	static ArrayList<EnemySpaceShip> easyEnemiesArray = new ArrayList<EnemySpaceShip>(); //easy enemies
	static ArrayList<EnemySpaceShip> intermediateEnemiesArray = new ArrayList<EnemySpaceShip>(); //intermediate enemies
	static ArrayList<EnemySpaceShip> advancedEnemiesArray = new ArrayList<EnemySpaceShip>(); //advanced enemies
	static ArrayList<PlayerSpaceship> players = new ArrayList<PlayerSpaceship>(); //player1 spaceships
	static ArrayList<Missiles> bulletObjects = new ArrayList<Missiles>(); //missiles

	//ArrayLists that holds barrier
	static ArrayList<Barrier> barrier1Array = new ArrayList<Barrier>(); //barrier1
	static ArrayList<Barrier> barrier2Array = new ArrayList<Barrier>(); //barrier2
	static ArrayList<Barrier> barrier3Array = new ArrayList<Barrier>(); //barrier3

	//creating object superEnemy which is type EnemySpaceShip
	Image enemyImage = new ImageIcon("SuperSpaceship.png").getImage();
	EnemySpaceShip superEnemy = new EnemySpaceShip(enemyImage);

	//whether game is paused
	private static boolean gamePaused = false;

	//whether game has started
	public boolean gameHasStarted = false;

	//spacing of enemies
	private int enemiesSpacingX = 60;
	private int enemiesSpacingY = 60;

	//enemies velocity
	private double enemyVelocityType1 = 1;
	private double enemyVelocityType2 = 1;

	//number of enemies per row
	private int numberOfEnemies = 10;

	//number of enemies alive
	private int numberOfEnemiesAlive = 0;

	//barrier width and height
	private int barrierElementWidth = 5;
	private int barrierElementHeight = 5;

	//missile width and height
	private int missileWidth = 5;
	private int missileHeight = 10;

	//enemy width and height
	private int enemyHeight = 50;
	private int enemyWidth = 50;

	//power up width and height
	private int powerUpHeight = 20;
	private int powerUpWidth = 20;

	//player1 width and height
	private int playerHeight = 50;
	private int playerWidth = 50;

	//barrier height and width
	private int barrierHeight = (30/barrierElementHeight);
	private int barrierWidth = (180/barrierElementWidth);

	//x position of top left pixel for each barrier
	private int barrier1XLeftPosition = 30;
	private int barrier2XLeftPosition = 310;
	private int barrier3XLeftPosition = 584;

	//y position of top left pixel for each barrier
	private int barrierTopYPosition = 420;

	//range for random numbers for super enemy
	private int minRandomNumber = 100;// 500;
	private int maxRandomNumber = 300;//5000;

	//range for random number for missiles
	private int minMissilesRandomNumber = 1;
	private int maxMissilesRandomNumber = 700;

	//stores the random number that is used to present the super enemy
	private int superEnemyRandomNumber = 0; //initially set to 0

	//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
	private int superEnemyCounter = 0; 

	//counter for missiles used in actionPerformed(), initially set to 0
	private int missilesCounter = 0;

	//level that the player1 is on
	private static int playerLevel = 1;

	//timer that invokes the action listener every 20ms
	Timer timer1 = new Timer(20, this);

	//string for location of enemies and barrier images
	private String noobImageLocation = "GreenSpaceship.png"; //green
	private String easyImageLocation = "YellowSpaceship.png"; //yellow
	private String intermediateImageLocation = "OrangeSpaceship.png"; //orange
	private String advancedImageLocation = "RedSpaceship.png"; //red
	private String barrierImageLocation = "DarkGreyRectangle.png"; //dark grey
	private String missileImageLocation = "PlayerMissile.png"; //pink
	
	// getting the image for the missile
	Image missileImage = new ImageIcon(missileImageLocation).getImage(); //image for Missile
	Missiles missile = new Missiles(missileImage, 0, 0); //creating object missile which is type Missiles 
	
	// getting the image for player1
	Image playerImage1 = new ImageIcon("BlueSpaceship.png").getImage(); //image for player1 spaceship
	PlayerSpaceship playership1 = new PlayerSpaceship(playerImage1, 0, 0); //creating object playership1 which is type PlayerSpaceship 
	
	// getting the image for player2
	Image playerImage2 = new ImageIcon("LightBlueSpaceship.png").getImage(); //image for player1 spaceship
	PlayerSpaceship playership2 = new PlayerSpaceship(playerImage2, 0, 0); //creating object playership1 which is type PlayerSpaceship 
	
	// to check if multiplayer game mode is selected
	private boolean multiplayer;
	
	// to check if the power-up that changes the speed of the player has been collected
	private boolean powered = false;
	// if that power-up has been collected, this checks to see if it reduces or increases the player speed
	private boolean speed = false;

	private boolean firstGame = true;
	private boolean levelFinished = false;

	static boolean multiplayerMode;

	static PlayerSpaceship player1; //player1
	static PlayerSpaceship player2; //player2

	//to check if the power-up has been collected
	public boolean collected = false;

	boolean playerHasPlayedMultiplayerBefore = false;

	private static boolean resetGame = false;

	static boolean hasGameBeenCompletelyInitialised = false;

	//Game class constructor
	public Game(boolean multi) {
		multiplayer = multi;

		//needed for threading
		playerLoop = new PlayerThread() {
			void update() { 
				updatePlayerObjects(); //updates Players movement and Animation
				updateBulletObjects(); //updates BulletObject movement and Animation
			}
		};

		if (multiplayer == true) {
			//creating object playership1 with starting positions 375 in the x axis, and 485 in y axis
			player1 = new PlayerSpaceship(playerImage1, 650, 480);
			player2 = new PlayerSpaceship(playerImage2, 100, 480);
			@SuppressWarnings("unused")
			GameKeyBindings gameKeyBindings = new GameKeyBindings(this, player1, player2);
			this.addGameObject(player1);
			this.addGameObject(player2);

			playerHasPlayedMultiplayerBefore = true;

		}
		else if (multiplayer == false) {
			//creating object playership1 with starting positions 375 in the x axis, and 485 in y axis
			player1 = new PlayerSpaceship(playerImage1, 375, 480);
			player2 = new PlayerSpaceship(playerImage2, 0, 0);
			player2.setVisibility(false);
			//creating GameKeyBindings
			@SuppressWarnings("unused")
			GameKeyBindings gameKeyBindings = new GameKeyBindings(this, player1, player2);
			this.addGameObject(player1);
			this.addGameObject(player2);
		}

		initialiseGame();
	}

	//initialises the game
	public void initialiseGame() {

		if (firstGame == false) {

			//makes the barriers from the previous level invisible
			for (Barrier barrier1 : barrier1Array) {
				barrier1.setVisibility(false);
			}

			for (Barrier barrier2 : barrier2Array) {
				barrier2.setVisibility(false);
			}

			for (Barrier barrier3 : barrier3Array) {
				barrier3.setVisibility(false);
			}

			//making all the remaining player1 missiles invisible
			for (Missiles bulletObject : bulletObjects) {
				if (bulletObject.getVisibility() == true) {
					bulletObject.setVisibility(false);
				}
			}

			//making all the remaining enemy missiles invisible
			for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
				if (advancedEnemy.missile.getVisibility() == true) {
					advancedEnemy.missile.setVisibility(false);
				}
			}

			for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
				if (intermediateEnemy.missile.getVisibility() == true) {
					intermediateEnemy.missile.setVisibility(false);
				}
			}

			for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
				if (easyEnemy.missile.getVisibility() == true) {
					easyEnemy.missile.setVisibility(false);
				}
			}

			for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
				if (noobEnemy.missile.getVisibility() == true) {
					noobEnemy.missile.setVisibility(false);
				}
			}
		}	

		if (playerLevel == 0) {
			playerLevel = 1;
		}

		//Looks at what level the is in and initialises that level accordingly 
		switch(playerLevel) {

		case 1:
			//playerLevel is 1, retrieving settings for level 1
			level1Settings();
			System.out.println("in level 1");
			break;

		case 2:
			//playerLevel is 2, retrieving settings for level 2
			level2Settings();
			System.out.println("in level 2");
			break;

		case 3:
			//playerLevel is 3, retrieving settings for level 3
			level3Settings();
			System.out.println("in level 3");
			break;

		case 4:
			//playerLevel is 4, retrieving settings for level 4
			level4Settings();
			System.out.println("in level 4");
			break;

		case 5:
			//playerLevel is 5, retrieving settings for level 5
			level5Settings();
			System.out.println("in level 5");
			break;
		}

		//methods that initiate enemies
		initiateNoobEnemies();
		initiateEasyEnemies();
		initiateIntermediateEnemies();
		initiateAdvancedEnemies();
		initiateSuperEnemy();

		//initialising new barriers
		initiateBarriers();

		//calling the setRandomNumber method
		superEnemyRandomNumber = setRandomNumber(minRandomNumber, maxRandomNumber); //setting a random number between min and max values
		System.out.println("Random number for superSpaceShip is: " + superEnemyRandomNumber + " ---"); //for debugging

		//adding gameKeyBindings to the Game jPanel 
		this.start();   
		
		
		//reset player horizontal speeds to original
		powered = false;

		pauseGame(playerLoop);
	}

	//contains settings for Level 1
	public void level1Settings() {
		//enemies velocity
		enemyVelocityType1 = 1;
		enemyVelocityType2 = 1;

		//number of enemies per row
		numberOfEnemies = 5;

		//spacing of enemies
		enemiesSpacingX = 100;
		enemiesSpacingY = 60;

		//number of enemies alive
		numberOfEnemiesAlive = numberOfEnemies*4;

		//barrier height and width
		barrierHeight = (20/barrierElementHeight);
		barrierWidth = (300/barrierElementWidth);

		//x position of top left pixel for each barrier
		barrier1XLeftPosition = 40;
		barrier2XLeftPosition = 460;
		barrier3XLeftPosition = 800; //barrier outside panel purposefully

		//y position of top left pixel for each barrier
		barrierTopYPosition = 420;

		//range for random numbers for super enemy
		minRandomNumber = 100;
		maxRandomNumber = 1000;

		//range for random number for missiles
		minMissilesRandomNumber = 1;
		maxMissilesRandomNumber = 300;

		//stores the random number that is used to present the super enemy
		superEnemyRandomNumber = 0; //initially set to 0

		//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
		superEnemyCounter = 0; 

		//counter for missiles used in actionPerformed(), initially set to 0
		missilesCounter = 0;

		playership1.setPoints(0); //setting player's points to zero
		playership1.setLives(2); //setting player's lives to 2

		if (firstGame == true) {
			firstGame = false;
			System.out.println("firstGame = false;");
		}
	}	

	//contains settings for Level 2
	public void level2Settings() {
		//enemies velocity
		enemyVelocityType1 = 1.5;
		enemyVelocityType2 = 1.5;

		//number of enemies per row
		numberOfEnemies = 8;

		//spacing of enemies
		enemiesSpacingX = 70;
		enemiesSpacingY = 60;

		//number of enemies alive
		numberOfEnemiesAlive = numberOfEnemies*4;

		//barrier height and width
		barrierHeight = (16/barrierElementHeight);
		barrierWidth = (180/barrierElementWidth);

		//x position of top left pixel for each barrier
		barrier1XLeftPosition = 34;
		barrier2XLeftPosition = 310;
		barrier3XLeftPosition = 580;

		//y position of top left pixel for each barrier
		barrierTopYPosition = 420;

		//range for random numbers for super enemy
		minRandomNumber = 200;
		maxRandomNumber = 1000;

		//range for random number for missiles
		minMissilesRandomNumber = 1;
		maxMissilesRandomNumber = 230;

		//stores the random number that is used to present the super enemy
		superEnemyRandomNumber = 0; //initially set to 0

		//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
		superEnemyCounter = 0; 

		//counter for missiles used in actionPerformed(), initially set to 0
		missilesCounter = 0;
	}

	//contains settings for Level 3
	public void level3Settings() {
		//enemies velocity
		enemyVelocityType1 = 1.75;
		enemyVelocityType2 = -1.75;

		//number of enemies per row
		numberOfEnemies = 10;

		//spacing of enemies
		enemiesSpacingX = 60;
		enemiesSpacingY = 60;

		//number of enemies alive
		numberOfEnemiesAlive = numberOfEnemies*4;

		//barrier height and width
		barrierHeight = (16/barrierElementHeight);
		barrierWidth = (150/barrierElementWidth);

		//x position of top left pixel for each barrier
		barrier1XLeftPosition = 88;
		barrier2XLeftPosition = 326;
		barrier3XLeftPosition = 564;

		//y position of top left pixel for each barrier
		barrierTopYPosition = 420;

		//range for random numbers for super enemy
		minRandomNumber = 500;
		maxRandomNumber = 2000;

		//range for random number for missiles
		minMissilesRandomNumber = 1;
		maxMissilesRandomNumber = 220;

		//stores the random number that is used to present the super enemy
		superEnemyRandomNumber = 0; //initially set to 0

		//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
		superEnemyCounter = 0; 

		//counter for missiles used in actionPerformed(), initially set to 0
		missilesCounter = 0;

	}

	//contains settings for Level 4
	public void level4Settings() {
		//enemies velocity
		enemyVelocityType1 = 2;
		enemyVelocityType2 = -2;

		//number of enemies per row
		numberOfEnemies = 11;

		//spacing of enemies
		enemiesSpacingX = 60;
		enemiesSpacingY = 60;

		//number of enemies alive
		numberOfEnemiesAlive = numberOfEnemies*4;

		//barrier height and width
		barrierHeight = (16/barrierElementHeight);
		barrierWidth = (200/barrierElementWidth);

		//x position of top left pixel for each barrier
		barrier1XLeftPosition = 0;
		barrier2XLeftPosition = 600;
		barrier3XLeftPosition = 900; //barrier outside panel purposefully

		//y position of top left pixel for each barrier
		barrierTopYPosition = 420;

		//range for random numbers for super enemy
		minRandomNumber = 500;//500;
		maxRandomNumber = 2000;//5000;

		//range for random number for missiles
		minMissilesRandomNumber = 1;
		maxMissilesRandomNumber = 160;

		//stores the random number that is used to present the super enemy
		superEnemyRandomNumber = 0; //initially set to 0

		//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
		superEnemyCounter = 0; 

		//counter for missiles used in actionPerformed(), initially set to 0
		missilesCounter = 0;
	}

	//contains settings for Level 5
	public void level5Settings() {
		//enemies velocity
		enemyVelocityType1 = 2.5;
		enemyVelocityType1 = -2.5;
		

		//number of enemies per row
		numberOfEnemies = 11;

		//spacing of enemies
		enemiesSpacingX = 60;
		enemiesSpacingY = 60;

		//number of enemies alive
		numberOfEnemiesAlive = numberOfEnemies*4;

		//barrier height and width
		barrierHeight = (16/barrierElementHeight);
		barrierWidth = (100/barrierElementWidth);

		//x position of top left pixel for each barrier
		barrier1XLeftPosition = 350;
		barrier2XLeftPosition = 900; //barrier outside panel purposefully
		barrier3XLeftPosition = 900; //barrier outside panel purposefully

		//y position of top left pixel for each barrier
		barrierTopYPosition = 420;

		//range for random numbers for super enemy
		minRandomNumber = 500;
		maxRandomNumber = 2000;

		//range for random number for missiles
		minMissilesRandomNumber = 1;
		maxMissilesRandomNumber = 150;

		//stores the random number that is used to present the super enemy
		superEnemyRandomNumber = 0; //initially set to 0

		//counter for SuperSpaceShip used in actionPerformed(), initially set to 0
		superEnemyCounter = 0; 

		//counter for missiles used in actionPerformed(), initially set to 0
		missilesCounter = 0;
	}

	//handles exiting the game
	public void endGameHandler() {
		//player1 still has lives remaining
		if (playership1.getLives() > 0 && playerLevel < 5) {

			//incrementing the level
			playerLevel++;

			levelFinished = true;

			//give player 1 additional lives
			playership1.increaseLife(1);

			//calling the initialiseGame method to set up next level
			initialiseGame();

			//player has lives remaining and has finished the game
		} else if (playership1.getLives() > 0 && playerLevel == 5) {

			//resetting player's level to 0 (needed for setting up the next game)
			playerLevel = 0;

			hasGameBeenCompletelyInitialised = false;

			//setting the visibility of player to invisible
			player1.setVisibility(false);
			player2.setVisibility(false);


			//Stopping track1 and playing the other track
			Sounds.stopClip1();
			try {
				Sounds.backgroundSong2();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Screens.changeToHighScoreExitScreen();

			//when player has no lives remaining (ie. dead)
		} else if (playership1.getLives() == 0) {

			//calling the resetGameToBeginning method
			resetGameToBeginning();

			//Stopping track2 and Playing the other track
			Screens.changeToNoHighScoreExitScreen();
			Sounds.stopClip1();
			try {
				Sounds.backgroundSong2();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//adds player/s to the screen
	public void addGameObject(PlayerSpaceship player) {
		players.add(player);
	}

	//adds missiles to the screen
	public void addGameObject(Missiles missile) {
		bulletObjects.add(missile);
	}

	//to get the size of an object
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}

	//updating the position of player1 spaceship
	private void updatePlayerObjects() {

		//array that contains one player1 spaceship
		ArrayList<PlayerSpaceship> playerObjects = new ArrayList<>(players);

		//to go through PlayerSpaceship array
		for (PlayerSpaceship player : playerObjects) {
			if (player.getVisibility() == true) {
				player.move(powered, speed); //moves player spaceship
			}
		}

	}

	//updating the position of player1 spaceship
	private void updateBulletObjects() {

		//array that contains one player1 spaceship
		ArrayList<Missiles> bulletObs = new ArrayList<>(bulletObjects);

		//to go through bullets array
		for (Missiles bulletObject : bulletObs) {
			if (bulletObject.getVisibility() == true) {
				bulletObject.move(); //moves bullets
			}
		}
	}

	//starts playerLoop thread
	public void start() {
		playerLoop.start();
	}	

	//creating 1 super enemy
	private void initiateSuperEnemy() {
		superEnemy.setXPosition(5);
		superEnemy.setYPosition(5);
		superEnemy.setXVelocity(2);
		superEnemy.setVisibility(false); //superEnemy not visible initially

		superEnemy.createPowerUp(); //creating superEnemey's Power Up
		superEnemy.powerUp.setVisibility(false); //making it invisible
	}

	//creating advanced enemies in one row
	private void initiateAdvancedEnemies() {
		for (int i = 0; i < numberOfEnemies; i++) {

			int tempNumber;

			//enemy initialisation
			Image enemyImage = new ImageIcon(advancedImageLocation).getImage();
			EnemySpaceShip advancedEnemy = new EnemySpaceShip(enemyImage); //advancedEnemy is an object of type EnemySpaceShip
			advancedEnemy.setXPosition(100 + enemiesSpacingX * i);
			advancedEnemy.setYPosition(enemiesSpacingY * 1);
			advancedEnemy.setXVelocity(enemyVelocityType2);

			//enemy's missile initialisation
			if (advancedEnemy.getVisibility() == true) {
				advancedEnemy.createMissile();
				advancedEnemy.missile.setVisibility(false); //initialling invisible
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				advancedEnemy.missile.setMissileRandomNumber(tempNumber); //setting random number for the missiles
			}

			advancedEnemiesArray.add(advancedEnemy);
		}	
	}

	//creating intermediate enemies in one row
	private void initiateIntermediateEnemies() {
		for (int i = 0; i < numberOfEnemies; i++) {

			int tempNumber;

			//enemy initialisation
			Image enemyImage = new ImageIcon(intermediateImageLocation).getImage();
			EnemySpaceShip intermediateEnemy = new EnemySpaceShip(enemyImage); //intermediateEnemy is an object of type EnemySpaceShip
			intermediateEnemy.setXPosition(100 + enemiesSpacingX * i);
			intermediateEnemy.setYPosition(enemiesSpacingY * 2);
			intermediateEnemy.setXVelocity(enemyVelocityType1);

			//enemy's missile initialisation
			if (intermediateEnemy.getVisibility() == true) {
				intermediateEnemy.createMissile();
				intermediateEnemy.missile.setVisibility(false); //initialling invisible
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				intermediateEnemy.missile.setMissileRandomNumber(tempNumber); //setting random number for the missiles
			}

			intermediateEnemiesArray.add(intermediateEnemy);
		}	
	}

	//creating easy enemies in one row
	private void initiateEasyEnemies() {
		for (int i = 0; i < numberOfEnemies; i++) {

			int tempNumber;

			//enemy initialisation
			Image enemyImage = new ImageIcon(easyImageLocation).getImage();
			EnemySpaceShip easyEnemy = new EnemySpaceShip(enemyImage); //easyEnemy is an object of type EnemySpaceShip
			easyEnemy.setXPosition(100 + enemiesSpacingX * i);
			easyEnemy.setYPosition(enemiesSpacingY * 3);
			easyEnemy.setXVelocity(enemyVelocityType2);

			//enemy's missile initialisation
			if (easyEnemy.getVisibility() == true) {
				easyEnemy.createMissile();
				easyEnemy.missile.setVisibility(false); //initialling invisible
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				easyEnemy.missile.setMissileRandomNumber(tempNumber); //setting random number for the missiles
			}

			easyEnemiesArray.add(easyEnemy);
		}	
	}

	//creating noob enemies in one row
	private void initiateNoobEnemies() {

		int tempNumber;

		for (int i = 0; i < numberOfEnemies; i++) {

			//enemy initialisation
			Image enemyImage = new ImageIcon(noobImageLocation).getImage();
			EnemySpaceShip noobEnemy = new EnemySpaceShip(enemyImage); //noobEnemy is an object of type EnemySpaceShip
			noobEnemy.setXPosition(100 + enemiesSpacingX * i);
			noobEnemy.setYPosition(enemiesSpacingY * 4);
			noobEnemy.setXVelocity(enemyVelocityType1);

			//enemy's missile initialisation
			if (noobEnemy.getVisibility() == true) {
				noobEnemy.createMissile();
				noobEnemy.missile.setVisibility(false); //initialling invisible
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				noobEnemy.missile.setMissileRandomNumber(tempNumber); //setting random number for the missiles
			}

			noobEnemiesArray.add(noobEnemy);
		}	
	}

	//creating 3 barriers: left, middle, right
	private void initiateBarriers() {

		//Barrier 1
		for (int i = 0; i <= barrierHeight; i++) {
			for (int j = 0; j <= barrierWidth; j++) {
				Image barrierImage = new ImageIcon(barrierImageLocation).getImage();
				Barrier barrier1 = new Barrier(barrierImage); //barrier1 is an object of type Barrier
				barrier1.setXPosition(barrier1XLeftPosition + (j*barrierElementWidth));
				barrier1.setYPosition(barrierTopYPosition + (i*barrierElementHeight));
				barrier1Array.add(barrier1);
			}	
		}

		//Barrier 2
		for (int i = 0; i <= barrierHeight; i++) {
			for (int j = 0; j <= barrierWidth; j++) {
				Image barrierImage = new ImageIcon(barrierImageLocation).getImage();
				Barrier barrier2 = new Barrier(barrierImage); //barrier1 is an object of type Barrier
				barrier2.setXPosition(barrier2XLeftPosition + (j*barrierElementWidth));
				barrier2.setYPosition(barrierTopYPosition + (i*barrierElementHeight));
				barrier2Array.add(barrier2);
			}	
		}

		//Barrier 3
		for (int i = 0; i <= barrierHeight; i++) {
			for (int j = 0; j <= barrierWidth; j++) {
				Image barrierImage = new ImageIcon(barrierImageLocation).getImage();
				Barrier barrier3 = new Barrier(barrierImage); //barrier1 is an object of type Barrier
				barrier3.setXPosition(barrier3XLeftPosition + (j*barrierElementWidth));
				barrier3.setYPosition(barrierTopYPosition + (i*barrierElementHeight));
				barrier3Array.add(barrier3);
			}	
		}
	}

	//Method that generates a random number between the range of min and max
	public int setRandomNumber(int min, int max) {
		int number;

		Random numberGenerator = new Random(); //creating a random number generator object

		//number returns a random number between the range, min is added to the value so that randomNumber > min
		number = min + (numberGenerator.nextInt((max - min) + 1));

		return number;
	}

	//used for painting the components that are on the screen
	public void paintComponent(Graphics g){

		numberOfEnemiesAlive = 0;

		//repainting the Game's background colour
		Color moccasin = new Color(255, 239, 213); //pastel light yellow

		this.setBackground(moccasin); 

		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		//calling collisionDetection method
		collisionDetection();

		if (levelFinished == true) {
			levelFinished = false;
		}

		//painting player1 spaceship

		if (player1.getVisibility() == true) {
			//draw the object to JPanel
			g2d.drawImage(playerImage1, (int) player1.getX(), (int) player1.getY(), null);
		}
	
		
		if (player2.getVisibility() == true) {
			//draw the object to JPanel
			g2d.drawImage(playerImage2, (int) player2.getX(), (int) player2.getY(), null);
		}

		//painting missiles
		for (Missiles bulletObject : bulletObjects) {
			if (bulletObject.getVisibility() == true) {
				//draw the object to JPanel
				g2d.drawImage(Missiles.getImage(), (int) bulletObject.getX(), (int) bulletObject.getY(), null);
			}
		}

		//draws every advanced enemy and their missile
		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			if (advancedEnemy.getVisibility() == true) {
				numberOfEnemiesAlive++; //incrementing the number of enemies that are alive
				g2d.drawImage(advancedEnemy.getImage(), Math.round(advancedEnemy.getXPosition()), Math.round(advancedEnemy.getYPosition()), this);	
				if (advancedEnemy.missile.getVisibility() == true) {
					g2d.drawImage(advancedEnemy.missile.getImage(), Math.round(advancedEnemy.missile.getXPosition()), Math.round(advancedEnemy.missile.getYPosition()), this);
				}
				//missile might still be displayed even if enemy is dead
			} else if (advancedEnemy.missile.getVisibility() == true) {
				g2d.drawImage(advancedEnemy.missile.getImage(), Math.round(advancedEnemy.missile.getXPosition()), Math.round(advancedEnemy.missile.getYPosition()), this);
			}
		}

		//draws every intermediate enemy and their missile
		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			if (intermediateEnemy.getVisibility() == true) {
				numberOfEnemiesAlive++; //incrementing the number of enemies that are alive
				g2d.drawImage(intermediateEnemy.getImage(), Math.round(intermediateEnemy.getXPosition()), Math.round(intermediateEnemy.getYPosition()), this);	
				if (intermediateEnemy.missile.getVisibility() == true) {
					g2d.drawImage(intermediateEnemy.missile.getImage(), Math.round(intermediateEnemy.missile.getXPosition()), Math.round(intermediateEnemy.missile.getYPosition()), this);
				}
				//missile might still be displayed even if enemy is dead
			} else if (intermediateEnemy.missile.getVisibility() == true) {
				g2d.drawImage(intermediateEnemy.missile.getImage(), Math.round(intermediateEnemy.missile.getXPosition()), Math.round(intermediateEnemy.missile.getYPosition()), this);
			} 
		}

		//draws every easy enemy and their missile
		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			if (easyEnemy.getVisibility() == true) {
				numberOfEnemiesAlive++; //incrementing the number of enemies that are alive
				g2d.drawImage(easyEnemy.getImage(), Math.round(easyEnemy.getXPosition()), Math.round(easyEnemy.getYPosition()), this);	
				if (easyEnemy.missile.getVisibility() == true) {
					g2d.drawImage(easyEnemy.missile.getImage(), Math.round(easyEnemy.missile.getXPosition()), Math.round(easyEnemy.missile.getYPosition()), this);
				}
				//missile might still be displayed even if enemy is dead
			} else if (easyEnemy.missile.getVisibility() == true) {
				g2d.drawImage(easyEnemy.missile.getImage(), Math.round(easyEnemy.missile.getXPosition()), Math.round(easyEnemy.missile.getYPosition()), this);
			}
		}

		//draws every noob enemy and their missile
		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			if (noobEnemy.getVisibility() == true) {
				numberOfEnemiesAlive++; //incrementing the number of enemies that are alive
				g2d.drawImage(noobEnemy.getImage(), Math.round(noobEnemy.getXPosition()), Math.round(noobEnemy.getYPosition()), this);	
				if (noobEnemy.missile.getVisibility() == true) {
					g2d.drawImage(noobEnemy.missile.getImage(), Math.round(noobEnemy.missile.getXPosition()), Math.round(noobEnemy.missile.getYPosition()), this);
				}
				//missile might still be displayed even if enemy is dead
			} else if (noobEnemy.missile.getVisibility() == true) {
				g2d.drawImage(noobEnemy.missile.getImage(), Math.round(noobEnemy.missile.getXPosition()), Math.round(noobEnemy.missile.getYPosition()), this);
			}
		}

		if (numberOfAliveEnemies() == 0) {
			//all enemies are dead, calling endGameHandler
			endGameHandler();
		}

		//draws super enemy 
		if (superEnemy.getVisibility() == true) {
			g2d.drawImage(superEnemy.getImage(), Math.round(superEnemy.getXPosition()), Math.round(superEnemy.getYPosition()), this);	
		}

		//if power up is visible, draws the power up
		if (superEnemy.powerUp.getVisibility() == true) {
			g2d.drawImage(superEnemy.powerUp.getImage(), Math.round(superEnemy.powerUp.getXPosition()), Math.round(superEnemy.powerUp.getYPosition()), this);		
		}

		//draws barrier1
		for (Barrier barrier1 : barrier1Array) {
			if (barrier1.getVisibility() == true) {
				g2d.drawImage(barrier1.getImage(), barrier1.getXPosition(), barrier1.getYPosition(), this);	
			}
		}

		//draws barrier2
		for (Barrier barrier2 : barrier2Array) {
			if (barrier2.getVisibility() == true) {
				g2d.drawImage(barrier2.getImage(), barrier2.getXPosition(), barrier2.getYPosition(), this);	
			}

		}

		//draws barrier3
		for (Barrier barrier3 : barrier3Array) {
			if (barrier3.getVisibility() == true) {
				g2d.drawImage(barrier3.getImage(), barrier3.getXPosition(), barrier3.getYPosition(), this);	
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		//checking if the resetGame variable is true
		if (resetGame == true) {
			resetGame = false;

			//initialisingGame to scratch as player playing the game again
			initialiseGame();
		}
		
		//checking if player has run out of lives, if they have, calling endGameHandler
		if (playership1.getLives() == 0) {
			endGameHandler();
		}

		//handling missileCounter
		if (missilesCounter <= maxMissilesRandomNumber) {
			missilesCounter++;
		} else {
			missilesCounter = 1;
		}

		if (superEnemyCounter == superEnemyRandomNumber) {
			superEnemy.setVisibility(true); //superEnemy now visible
			superEnemy.setXVelocityChanges(0); //no changes to begin with
			superEnemy.setXVelocity(5); //set x velocity of superEnemy to 5
			superEnemyRandomNumber = 0;

		} else if (superEnemy.getXVelocityChanges() == 2) { //waiting for when the enemy has traveled to the right edge and come back
			superEnemy.setVisibility(false); //superEnemy not visible
			superEnemy.setXPosition(5); //reset superEnemy to original position
			superEnemy.setXVelocity(0); //set x velocity of superEnemy to 0- stationary
			superEnemyRandomNumber = setRandomNumber(minRandomNumber, maxRandomNumber); //resetting randomNumber to a new random number
			superEnemy.setXVelocityChanges(0); //no changes to begin with
			superEnemyCounter = 1;
			System.out.println("Random number for superSpaceShip is: " + superEnemyRandomNumber + " ---"); //for debugging

		} else {
			superEnemyCounter++;
		}

		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			noobEnemy.updatePosition(); //calls the updatePosition method for enemy spaceship

			if ((noobEnemy.missile.getMissileRandomNumber() == missilesCounter) && (noobEnemy.getVisibility() == true)) {
				noobEnemy.missile.setXPosition(noobEnemy.getXPosition() + 25);
				noobEnemy.missile.setYPosition(noobEnemy.getYPosition() + 40);
				noobEnemy.missile.setVisibility(true);
			} else if (noobEnemy.missile.getVisibility() == true) {
				noobEnemy.missile.updatePosition(); //updating position of missile because it is visible
			}
		}

		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			easyEnemy.updatePosition(); //calls the updatePosition method for enemy spaceship

			if ((easyEnemy.missile.getMissileRandomNumber() == missilesCounter) && (easyEnemy.getVisibility() == true)) {
				easyEnemy.missile.setXPosition(easyEnemy.getXPosition() + 25);
				easyEnemy.missile.setYPosition(easyEnemy.getYPosition() + 40);
				easyEnemy.missile.setVisibility(true);
			} else if (easyEnemy.missile.getVisibility() == true) {
				easyEnemy.missile.updatePosition(); //updating position of missile because it is visible
			}
		}

		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			intermediateEnemy.updatePosition(); //calls the updatePosition method for enemy spaceship

			if ((intermediateEnemy.missile.getMissileRandomNumber() == missilesCounter) && (intermediateEnemy.getVisibility() == true)) {
				intermediateEnemy.missile.setXPosition(intermediateEnemy.getXPosition() + 25);
				intermediateEnemy.missile.setYPosition(intermediateEnemy.getYPosition() + 40);
				intermediateEnemy.missile.setVisibility(true);
			} else if (intermediateEnemy.missile.getVisibility() == true) {
				intermediateEnemy.missile.updatePosition(); //updating position of missile because it is visible
			}
		}

		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			advancedEnemy.updatePosition(); //calls the updatePosition method for enemy spaceship

			if ((advancedEnemy.missile.getMissileRandomNumber() == missilesCounter) && (advancedEnemy.getVisibility() == true)) {
				advancedEnemy.missile.setXPosition(advancedEnemy.getXPosition() + 25);
				advancedEnemy.missile.setYPosition(advancedEnemy.getYPosition() + 40);
				advancedEnemy.missile.setVisibility(true);
			} else if (advancedEnemy.missile.getVisibility() == true) {
				advancedEnemy.missile.updatePosition(); //updating position of missile because it is visible
			}
		}

		//if superEnemy is visible, update its position
		if (superEnemy.getVisibility() == true) {
			superEnemy.updatePosition();
		}

		//if superEnemy's powerup is visible, update its position
		if (superEnemy.powerUp.getVisibility() == true) {
			superEnemy.powerUp.updatePosition();
		}

		repaint(); //calls the paintComponent method
	}

	//subclass GameKeyBinders
	
	class GameKeyBindings {

		GameKeyBindings(final Game game, final PlayerSpaceship player1) {

			//right key (to move right)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_RIGHT, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					if (hasGameStarted() == true) {
						player1.setRight(true); // sets right movement true to move the player when the key is pressed
					}
				}
			}, "right pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player1.setRight(false); // sets right movement false to stop the player from moving when the key is released
				}
			}, "right released");

			//left key (to move left)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_LEFT, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					if (hasGameStarted() == true) {
						player1.setLeft(true); // sets left movement true to move the player when the key is pressed
					}
				}
			}, "left pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player1.setLeft(false); // sets left movement false to stop the player from moving when the key is released
				}
			}, "left released");

			//shot key (to shoot)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_UP, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					
					//start the game if it has not been stated
					if (hasGameStarted() == false) {
						resumeGame(playerLoop);
						gameHasStarted = true;
						System.out.println("Game has now started");

					} else if (hasGameStarted() == true) {
						if ((player1.getShot() == true) || player1.getVisibility() == false) { // if player is holding down the up arrow key to shoot or if he/she is dead do nothing
							return;
						}
					}

					Missiles bullet = new Missiles(missileImage, (int) (player1.getX() + (playerWidth/2 - missileWidth/2)), (int) (player1.getY()));
					game.addGameObject(bullet);//add the bullet to the List of GameObjects that will be drawn on next screen paint
					player1.setShot(true); // this will make sure bullets are not spammed if the player holds down the key to shoot as the play can only shoot when this is false initially, so only one bullet will appear per key press
					if(!state.getPauseStatus()){
						//playing the sound for the missile
						try {
							Sounds.missileSounds();
						} catch (UnsupportedAudioFileException ee) {
							ee.printStackTrace();
						} catch (IOException ee) {
							ee.printStackTrace();
						} catch (LineUnavailableException ee) {
							ee.printStackTrace();
						}
					}
				}
			}, "up pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player1.setShot(false); // the player can once again shoot upon the next key press, to make sure only one bullet can be shot per key press

				}
			}, "up released");

			//p key (to pause game)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_P, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					
					// if game is running pause the game
					if (gamePaused == false) {
						pauseGame(playerLoop);						
						if (hasGameStarted() == false) {
							gameHasStarted = true;
						}
					} 
					//if the game is paused resume the game
					else if (gamePaused == true) {
						resumeGame(playerLoop);						
					}

				}
			}, "p pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {

				}
			}, "p released");

			//o key (to options screen)
			//OPENS THE OPTION MENU WHEN THE 'O' KEY IS PRESSED AT ANY POINT IN THE APPLICATION
			//Also pauses the game
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_O, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {

					pauseGame(playerLoop);
					Screens.goToOptionScreenFromGameScreen();

				}
			}, "o pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {

				}
			}, "o released");

		}
		
		// adds keys for the second player
		GameKeyBindings(final Game game, PlayerSpaceship player1, final PlayerSpaceship player2) {
			this(game, player1);

			//x key (to move right)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_X, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					if (hasGameStarted() == true) {
						player2.setRight(true); // sets right movement true to move the player when the key is pressed
					}
				}
			}, "x pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player2.setRight(false); // sets right movement false to stop the player from moving when the key is released
				}
			}, "x released");

			//left key (to move left)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_Z, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					if (hasGameStarted() == true) {
						player2.setLeft(true); // sets left movement true to move the player when the key is pressed
					}
				}
			}, "z pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player2.setLeft(false); // sets left movement false to stop the player from moving when the key is released
				}
			}, "z released");

			//space bar (to shoot)
			KeyBinding.addKeyBinding(game, KeyBinding.WHEN_IN_FOCUSED_WINDOW, KeyEvent.VK_SPACE, new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					
					//if the game has not started start the game
					if (hasGameStarted() == false) {
						resumeGame(playerLoop);
						gameHasStarted = true;
						System.out.println("Game has now started");

					} else if (hasGameStarted() == true) {

						if ((player2.getShot() == true) || player2.getVisibility() == false) { // if player is holding down the space bar to shoot or if he/she is dead do nothing
							return;
						}

						Missiles bullet = new Missiles(missileImage, (int) (player2.getX() + (playerWidth/2 - missileWidth/2)), (int) (player2.getY()));// + (playerHeight/2 - missileWidth/2)), true);	
						game.addGameObject(bullet);//add the bullet to the List of GameObjects that will be drawn on next screen paint
						player2.setShot(true); // this will make sure bullets are not spammed if the player holds down the key to shoot as the player can only shoot when this is false initially, so only one bullet will appear per key press	
						if(!state.getPauseStatus()){
							//again playing the sound for the missile with different conditions 
							try {
								Sounds.missileSounds();
							} catch (UnsupportedAudioFileException ee) {
								ee.printStackTrace();
							} catch (IOException ee) {
								ee.printStackTrace();
							} catch (LineUnavailableException ee) {
								ee.printStackTrace();
							}
						}
					}
				}
			}, "Space pressed", new AbstractAction() {

				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent e) {
					player2.setShot(false); // the player can once again shoot upon the next key press, to make sure only one bullet can be shot per key press

				}
			}, "Space released");

		}
	}

	//static subclass KeyBinding
	// some ideas gotten from https://www.daniweb.com/software-development/java/code/444547/game-development-loop-logic-and-collision-detection-java-swing-2d
	static class KeyBinding {

		JComponent gameComp;
		int inputMap;
		public static final int WHEN_IN_FOCUSED_WINDOW = JComponent.WHEN_IN_FOCUSED_WINDOW;

		public KeyBinding(JComponent gameComp, int inputMap) {
			this.gameComp = gameComp;
			this.inputMap = inputMap;
		}
		
		// sets the actions for each key
		public static void putKeyBinding(JComponent gameComp, int inputMap, int key, boolean onRelease, AbstractAction keybindingAction, String description) {
			gameComp.getInputMap(inputMap).put(KeyStroke.getKeyStroke(key, 0, onRelease), description);
			gameComp.getActionMap().put(description, keybindingAction);
		}
		// adds keys to the component which is the player
		public static void addKeyBinding(JComponent gameComp, int inputMap, int key, AbstractAction onPressAction, String onPressDesc, AbstractAction onReleaseAction, String onReleaseDesc) {
			putKeyBinding(gameComp, inputMap, key, false, onPressAction, onPressDesc);
			putKeyBinding(gameComp, inputMap, key, true, onReleaseAction, onReleaseDesc);
		}	
	}

	//method the detects collisions
	@SuppressWarnings("unused")
	private void collisionDetection() {

		Collections.reverse(barrier1Array); //reversing the order of barrier1Array

		//goes through each barrier1 object and checks whether the players missile's has collided
		for (Barrier barrier1 : barrier1Array) {
			if (barrier1.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {
						//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() + 4 >= barrier1.getXPosition()) && ((bulletObject.getX() <= barrier1.getXPosition() + barrierElementWidth) || (bulletObject.getX() + 8 <= barrier1.getXPosition()) ) && (bulletObject.getY()  <= barrier1.getYPosition() + barrierElementHeight)) { 
							//bullet has hit get rid of it
							barrier1.setVisibility(false);
							bulletObject.setVisibility(false);
						}
					}
				}
			}
		}

		Collections.reverse(barrier1Array); //reversing the order of barrier1Array again so that it is back to original 
		Collections.reverse(barrier2Array); //reversing the order of barrier2Array

		//goes through each barrier2 object and checks whether the players missile's has collided
		for (Barrier barrier2 : barrier2Array) {
			if (barrier2.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {
						//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() + 4 >= barrier2.getXPosition()) && ((bulletObject.getX() <= barrier2.getXPosition() + barrierElementWidth) || (bulletObject.getX() + 8 <= barrier2.getXPosition() ) ) && (bulletObject.getY()  <= barrier2.getYPosition() + barrierElementHeight)) {
							//bullet has hit get rid of it
							barrier2.setVisibility(false);
							bulletObject.setVisibility(false);
						}
					}
				}
			}
		}

		Collections.reverse(barrier2Array); //reversing the order of barrier2Array again so that it is back to original 
		Collections.reverse(barrier3Array); //reversing the order of barrier3Array

		//goes through each barrier3 object and checks whether the players missile's has collided
		for (Barrier barrier3 : barrier3Array) {
			if (barrier3.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {
						//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() + 4 >= barrier3.getXPosition()) && ((bulletObject.getX() <= barrier3.getXPosition() + barrierElementWidth) || (bulletObject.getX() + 8 <= barrier3.getXPosition() ) ) && (bulletObject.getY() <= barrier3.getYPosition() + barrierElementHeight)) {
							//bullet has hit get rid of it
							barrier3.setVisibility(false);
							bulletObject.setVisibility(false);
						}
					}
				}
			}
		}

		Collections.reverse(barrier3Array); //reversing the order of barrier3Array again so that it is back to original 

		//checking enemies and whether the players missile's has collided
		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			if (noobEnemy.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() >= noobEnemy.getXPosition()) && (bulletObject.getX() <= noobEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= noobEnemy.getYPosition()) && (bulletObject.getY() <= noobEnemy.getYPosition() + enemyHeight)) {
							//bullet has hit get rid of it
							noobEnemy.setVisibility(false);
							bulletObject.setVisibility(false);
							playership1.increasePoints(10);
						}
					}
				}
			}
		}

		//checking enemies and whether the players missile's has collided
		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			if (easyEnemy.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() >= easyEnemy.getXPosition()) && (bulletObject.getX() <= easyEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= easyEnemy.getYPosition()) && (bulletObject.getY() <= easyEnemy.getYPosition() + enemyHeight) && (easyEnemy.getHitCount() < 1)) {
							//bullet has hit get rid of it
							easyEnemy.setHitCount(1);
							bulletObject.setVisibility(false);
						}
						else if ((bulletObject.getX() >= easyEnemy.getXPosition()) && (bulletObject.getX() <= easyEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= easyEnemy.getYPosition()) && (bulletObject.getY() <= easyEnemy.getYPosition() + enemyHeight) && (easyEnemy.getHitCount() == 1)) {
							easyEnemy.setVisibility(false);
							bulletObject.setVisibility(false);
							playership1.increasePoints(20);
						}
					}
				}
			}
		}

		//checking enemies and whether the players missile's has collided
		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			if (intermediateEnemy.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() >= intermediateEnemy.getXPosition()) && (bulletObject.getX() <= intermediateEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= intermediateEnemy.getYPosition()) && (bulletObject.getY() <= intermediateEnemy.getYPosition() + enemyHeight) && (intermediateEnemy.getHitCount() < 2)) {
							//bullet has hit get rid of it
							intermediateEnemy.setHitCount(1);
							bulletObject.setVisibility(false);
						}
						else if ((bulletObject.getX() >= intermediateEnemy.getXPosition()) && (bulletObject.getX() <= intermediateEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= intermediateEnemy.getYPosition()) && (bulletObject.getY() <= intermediateEnemy.getYPosition() + enemyHeight) && (intermediateEnemy.getHitCount() == 2)) {
							intermediateEnemy.setVisibility(false);
							bulletObject.setVisibility(false);
							playership1.increasePoints(30);
						}
					}
				}
			}
		}

		//checking enemies and whether the players missile's has collided
		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			if (advancedEnemy.getVisibility() == true) {
				for (Missiles bulletObject : bulletObjects) {
					if (bulletObject.getVisibility() == true) {//check if the bullets intersects any other object besides its owner
						if ((bulletObject.getX() >= advancedEnemy.getXPosition()) && (bulletObject.getX() <= advancedEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= advancedEnemy.getYPosition()) && (bulletObject.getY() <= advancedEnemy.getYPosition() + enemyHeight) && (advancedEnemy.getHitCount() < 3)) {
							//bullet has hit get rid of it
							advancedEnemy.setHitCount(1);
							bulletObject.setVisibility(false);
						}
						else if ((bulletObject.getX() >= advancedEnemy.getXPosition()) && (bulletObject.getX() <= advancedEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() >= advancedEnemy.getYPosition()) && (bulletObject.getY() <= advancedEnemy.getYPosition() + enemyHeight) && (advancedEnemy.getHitCount() == 3)) {
							advancedEnemy.setVisibility(false);
							bulletObject.setVisibility(false);
							playership1.increasePoints(40);
						}
					}
				}
			}
		}

		//checking enemies and whether the players missile's has collided
		if (superEnemy.getVisibility() == true) {
			for (Missiles bulletObject : bulletObjects) {
				if (bulletObject.getVisibility() == true) {//check if the bullets intersects any other object besides its owner
					if ((bulletObject.getX() >= superEnemy.getXPosition()) && (bulletObject.getX() <= superEnemy.getXPosition() + enemyWidth) && (bulletObject.getY() <= superEnemy.getYPosition() + enemyHeight)) {
						//bullet has hit get rid of it

						int tempNumber;

						bulletObject.setVisibility(false);
						superEnemy.setVisibility(false);
						playership1.increasePoints(100);

						//power up
						superEnemy.powerUp.setXPosition(superEnemy.getXPosition() + 25);
						superEnemy.powerUp.setYPosition(superEnemy.getYPosition() + 40);
						superEnemy.powerUp.setVisibility(true); //making it visible

						superEnemy.setXPosition(5); //reset superEnemy to original position
						superEnemy.setXVelocity(0); //set x velocity of superEnemy to 0- stationary
						superEnemyRandomNumber = setRandomNumber(minRandomNumber, maxRandomNumber); //resetting randomNumber to a new random number
						superEnemy.setXVelocityChanges(0); //no changes to begin with
						superEnemyCounter = 1;
						System.out.println("Random number for superSpaceShip is: " + superEnemyRandomNumber + " ---"); //for debugging

						//the sound is played when super spaceship gets hit
						try {
							Sounds.playerIsShot();
						} catch (UnsupportedAudioFileException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (LineUnavailableException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}

		//checking whether enemies' missile has collided with anything
		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {//go through each object again
			//make sure we are not checking the bullet against itself
			if ((advancedEnemy.missile.getVisibility() == true)) { //&& (advancedEnemy.getVisibility() == true)) {

				for (Barrier barrier3 : barrier3Array) {
					if (barrier3.getVisibility() == true) {
						if ((advancedEnemy.missile.getXPosition() >= barrier3.getXPosition()) && (advancedEnemy.missile.getXPosition() <= barrier3.getXPosition() + barrierElementWidth) && (advancedEnemy.missile.getYPosition() + missileHeight >= barrier3.getYPosition())) {
							//bullet has hit get rid of it
							advancedEnemy.missile.setVisibility(false);
							barrier3.setVisibility(false);
						}
					}
				}

				for (Barrier barrier2 : barrier2Array) {
					if (barrier2.getVisibility() == true) {
						if ((advancedEnemy.missile.getXPosition() >= barrier2.getXPosition()) && (advancedEnemy.missile.getXPosition() <= barrier2.getXPosition() + barrierElementWidth) && (advancedEnemy.missile.getYPosition() + missileHeight >= barrier2.getYPosition())) {
							//bullet has hit get rid of it
							advancedEnemy.missile.setVisibility(false);
							barrier2.setVisibility(false);
						}
					}
				}

				for (Barrier barrier1 : barrier1Array) {
					if (barrier1.getVisibility() == true) {
						if ((advancedEnemy.missile.getXPosition() >= barrier1.getXPosition()) && (advancedEnemy.missile.getXPosition() <= barrier1.getXPosition() + barrierElementWidth) && (advancedEnemy.missile.getYPosition() + missileHeight >= barrier1.getYPosition())) {
							//bullet has hit get rid of it
							advancedEnemy.missile.setVisibility(false);
							barrier1.setVisibility(false);
						}
					}
				}

				for (PlayerSpaceship player1 : players) {
					if (player1.getVisibility() == true) {
						if (((advancedEnemy.missile.getXPosition() + missileWidth) >= player1.getX()) && (advancedEnemy.missile.getXPosition() <= player1.getX() + playerWidth) && (advancedEnemy.missile.getYPosition() + missileHeight >= player1.getY()) && (advancedEnemy.missile.getYPosition() <= player1.getY() + playerHeight)) {
							advancedEnemy.missile.setVisibility(false);
							if (playership1.getLives() > 1) {
								playership1.decreaseLife(); //decrease the player's lives
							} else {
								playership1.decreaseLife(); //decrease the player's lives
								player1.setVisibility(false); //setting the player's visibility to false
								endGameHandler(); //calling the endGameHandler method
							}						
						}
					}
				}
			}
		}

		//checking whether enemies' missile has collided with anything
		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {//go through each object again
			//make sure we are not checking the bullet against itself
			if ((intermediateEnemy.missile.getVisibility() == true)) {// && (intermediateEnemy.getVisibility() == true)) {

				for (Barrier barrier3 : barrier3Array) {
					if (barrier3.getVisibility() == true) {
						if ((intermediateEnemy.missile.getXPosition() >= barrier3.getXPosition()) && (intermediateEnemy.missile.getXPosition() <= barrier3.getXPosition() + barrierElementWidth) && (intermediateEnemy.missile.getYPosition() + missileHeight >= barrier3.getYPosition())) {
							//bullet has hit get rid of it
							intermediateEnemy.missile.setVisibility(false);
							barrier3.setVisibility(false);
						}
					}
				}

				for (Barrier barrier2 : barrier2Array) {
					if (barrier2.getVisibility() == true) {
						if ((intermediateEnemy.missile.getXPosition() >= barrier2.getXPosition()) && (intermediateEnemy.missile.getXPosition() <= barrier2.getXPosition() + barrierElementWidth) && (intermediateEnemy.missile.getYPosition() + missileHeight >= barrier2.getYPosition())) {
							//bullet has hit get rid of it
							intermediateEnemy.missile.setVisibility(false);
							barrier2.setVisibility(false);
						}
					}
				}

				for (Barrier barrier1 : barrier1Array) {
					if (barrier1.getVisibility() == true) {
						if ((intermediateEnemy.missile.getXPosition() >= barrier1.getXPosition()) && (intermediateEnemy.missile.getXPosition() <= barrier1.getXPosition() + barrierElementWidth) && (intermediateEnemy.missile.getYPosition() + missileHeight >= barrier1.getYPosition())) {
							//bullet has hit get rid of it
							intermediateEnemy.missile.setVisibility(false);
							barrier1.setVisibility(false);
						}
					}
				}

				for (PlayerSpaceship player1 : players) {
					if (player1.getVisibility() == true) {
						if (((intermediateEnemy.missile.getXPosition() + missileWidth) >= player1.getX()) && (intermediateEnemy.missile.getXPosition() <= player1.getX() + playerWidth) && (intermediateEnemy.missile.getYPosition() + missileHeight >= player1.getY()) && (intermediateEnemy.missile.getYPosition() <= player1.getY() + playerHeight)) {
							intermediateEnemy.missile.setVisibility(false);
							if (playership1.getLives() > 1) {
								playership1.decreaseLife(); //decrease the player's lives
							} else {
								playership1.decreaseLife(); //decrease the player's lives
								player1.setVisibility(false); //setting the player's visibility to false
								endGameHandler(); //calling the endGameHandler method
							}						
						}
					}
				}

			}
		}

		//checking whether enemies' missile has collided with anything
		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {//go through each object again
			//make sure we are not checking the bullet against itself
			if ((easyEnemy.missile.getVisibility() == true)) {

				for (Barrier barrier3 : barrier3Array) {
					if (barrier3.getVisibility() == true) {
						if ((easyEnemy.missile.getXPosition() >= barrier3.getXPosition()) && (easyEnemy.missile.getXPosition() <= barrier3.getXPosition() + barrierElementWidth) && (easyEnemy.missile.getYPosition() + missileHeight >= barrier3.getYPosition())) {
							//bullet has hit get rid of it
							easyEnemy.missile.setVisibility(false);
							barrier3.setVisibility(false);
						}
					}
				}

				for (Barrier barrier2 : barrier2Array) {
					if (barrier2.getVisibility() == true) {
						if ((easyEnemy.missile.getXPosition() >= barrier2.getXPosition()) && (easyEnemy.missile.getXPosition() <= barrier2.getXPosition() + barrierElementWidth) && (easyEnemy.missile.getYPosition() + missileHeight >= barrier2.getYPosition())) {
							//bullet has hit get rid of it
							easyEnemy.missile.setVisibility(false);
							barrier2.setVisibility(false);
						}
					}
				}

				for (Barrier barrier1 : barrier1Array) {
					if (barrier1.getVisibility() == true) {
						if ((easyEnemy.missile.getXPosition() >= barrier1.getXPosition()) && (easyEnemy.missile.getXPosition() <= barrier1.getXPosition() + barrierElementWidth) && (easyEnemy.missile.getYPosition() + missileHeight >= barrier1.getYPosition())) {
							//bullet has hit get rid of it
							easyEnemy.missile.setVisibility(false);
							barrier1.setVisibility(false);
						}
					}
				}

				for (PlayerSpaceship player1 : players) {
					if (player1.getVisibility() == true) {
						if (((easyEnemy.missile.getXPosition() + missileWidth) >= player1.getX()) && (easyEnemy.missile.getXPosition() <= player1.getX() + playerWidth) && (easyEnemy.missile.getYPosition() + missileHeight >= player1.getY()) && (easyEnemy.missile.getYPosition() <= player1.getY() + playerHeight)) {
							easyEnemy.missile.setVisibility(false);
							if (playership1.getLives() > 1) {
								playership1.decreaseLife(); //decrease the player's lives
							} else {
								playership1.decreaseLife(); //decrease the player's lives
								player1.setVisibility(false); //setting the player's visibility to false
								endGameHandler(); //calling the endGameHandler method
							}					
						}
					}
				}

			}
		}

		//checking whether enemies' missile has collided with anything
		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {//go trhough each object again
			//make sure we are not checking the bullet against itself
			if ((noobEnemy.missile.getVisibility() == true)) {// && (noobEnemy.getVisibility() == true)) {

				for (Barrier barrier3 : barrier3Array) {
					if (barrier3.getVisibility() == true) {
						if ((noobEnemy.missile.getXPosition() >= barrier3.getXPosition()) && (noobEnemy.missile.getXPosition() <= barrier3.getXPosition() + barrierElementWidth) && (noobEnemy.missile.getYPosition() + missileHeight >= barrier3.getYPosition())) {
							//bullet has hit get rid of it
							noobEnemy.missile.setVisibility(false);
							barrier3.setVisibility(false);
						}
					}
				}


				for (Barrier barrier2 : barrier2Array) {
					if (barrier2.getVisibility() == true) {
						if ((noobEnemy.missile.getXPosition() >= barrier2.getXPosition()) && (noobEnemy.missile.getXPosition() <= barrier2.getXPosition() + barrierElementWidth) && (noobEnemy.missile.getYPosition() + missileHeight >= barrier2.getYPosition())) {
							//bullet has hit get rid of it
							noobEnemy.missile.setVisibility(false);
							barrier2.setVisibility(false);
						}
					}
				}

				for (Barrier barrier1 : barrier1Array) {
					if (barrier1.getVisibility() == true) {
						if ((noobEnemy.missile.getXPosition() >= barrier1.getXPosition()) && (noobEnemy.missile.getXPosition() <= barrier1.getXPosition() + barrierElementWidth) && (noobEnemy.missile.getYPosition() + missileHeight >= barrier1.getYPosition())) {
							//bullet has hit get rid of it
							noobEnemy.missile.setVisibility(false);
							barrier1.setVisibility(false);
						}
					}
				}

				for (PlayerSpaceship player1 : players) {
					if (player1.getVisibility() == true) {
						if (((noobEnemy.missile.getXPosition() + missileWidth) >= player1.getX()) && (noobEnemy.missile.getXPosition() <= player1.getX() + playerWidth) && (noobEnemy.missile.getYPosition() + missileHeight >= player1.getY()) && (noobEnemy.missile.getYPosition() <= player1.getY() + playerHeight)) {							noobEnemy.missile.setVisibility(false);
						if (playership1.getLives() > 1) {
							playership1.decreaseLife(); //decrease the player's lives
						} else {
							playership1.decreaseLife(); //decrease the player's lives
							player1.setVisibility(false); //setting the player's visibility to false
							endGameHandler(); //calling the endGameHandler method
						}							
						}
					}
				}

			}
		}

		//checking if the player has collided with the power up
		if (superEnemy.powerUp.getVisibility() == true) {

			//getting rid of all the barriers in its path
			for (Barrier barrier3 : barrier3Array) {
				if (barrier3.getVisibility() == true) {
					if ((superEnemy.powerUp.getXPosition() + powerUpWidth >= barrier3.getXPosition()) && (superEnemy.powerUp.getXPosition() <= barrier3.getXPosition() + barrierElementWidth) && (superEnemy.powerUp.getYPosition() + powerUpHeight >= barrier3.getYPosition())) {
						//power up has hit get rid of it
						barrier3.setVisibility(false);
					}
				}
			}

			for (Barrier barrier2 : barrier2Array) {
				if (barrier2.getVisibility() == true) {
					if ((superEnemy.powerUp.getXPosition() + powerUpWidth >= barrier2.getXPosition()) && (superEnemy.powerUp.getXPosition() <= barrier2.getXPosition() + barrierElementWidth) && (superEnemy.powerUp.getYPosition() + powerUpHeight >= barrier2.getYPosition())) {
						//power up has hit get rid of it
						barrier2.setVisibility(false);
					}
				}
			}

			for (Barrier barrier1 : barrier1Array) {
				if (barrier1.getVisibility() == true) {
					if ((superEnemy.powerUp.getXPosition() + powerUpWidth >= barrier1.getXPosition()) && (superEnemy.powerUp.getXPosition() <= barrier1.getXPosition() + barrierElementWidth) && (superEnemy.powerUp.getYPosition() + powerUpHeight >= barrier1.getYPosition())) {
						//power up has hit get rid of it
						barrier1.setVisibility(false);
					}
				}
			}

			for (PlayerSpaceship player1 : players) {
				if (player1.getVisibility() == true) {
					if (((superEnemy.powerUp.getXPosition() + powerUpWidth) >= player1.getX()) && (superEnemy.powerUp.getXPosition() <= player1.getX() + playerWidth) && (superEnemy.powerUp.getYPosition() + powerUpHeight >= player1.getY()) && (superEnemy.powerUp.getYPosition() <= player1.getY() + playerHeight)) {
						superEnemy.powerUp.setVisibility(false);
						collected = true;
						//heart beat sound to signify power up collected
						try {
							Sounds.powerUpCollected();
						} catch (UnsupportedAudioFileException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						} catch (LineUnavailableException e) {
							e.printStackTrace();
						}
						tempNumber = setRandomNumber(0, 6); //there are 7 powerups
						superEnemy.powerUp.setPowerUpRandomNumber(tempNumber); //setting random number for the missiles

						System.out.println("power up#: " + superEnemy.powerUp.getPowerUpRandomNumber());

						initiatePowerUp(tempNumber); //calling the initiatePowerUp function
					}
				}							
			}
		}
	}

	//returns the pause state of the game
	public boolean getGamePauseState() {
		return gamePaused;
	}

	//returns the level that the player is currently on
	public int getPlayerLevel() {

		return playerLevel;
	}

	//sets the level for the player
	public static void setPlayerLevel(int level) {

		playerLevel = level;
	}

	//whether player has finished a level
	public boolean getLevelStatus() {
		return levelFinished;
	}

	//pauses game
	public void pauseGame(final PlayerThread Thread) {
		timer1.stop(); //stopping the timer
		Thread.pausing(); //pausing the thread
		gamePaused = true; //setting the pause state to true
		state.setPauseStatus(true); //for screens
	}

	//resumes game
	public void resumeGame(final PlayerThread Thread) {
		timer1.start(); //starting the timer
		Thread.start(); //starting thread
		gamePaused = false; //setting the pause state to false
		state.setPauseStatus(false); //for screens
	}

	//returns whether player has started playing the game
	public boolean hasGameStarted() {
		return gameHasStarted;
	}

	//returns the number of enemies that are alive
	public int numberOfAliveEnemies() {
		return numberOfEnemiesAlive;
	}

	//initiates and handles all the powerups
	public void initiatePowerUp(int number) {

		int tempNumber;

		//handles all of the potential Power Ups based on the random number generated
		switch(number) {

		case 0:
			//increases the rate at which enemies can shoot

			maxMissilesRandomNumber = 400; //decreasing the range whereby a random number is allocated

			for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				noobEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				easyEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				intermediateEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				advancedEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			break;

		case 1:
			//decreases the rate at which enemies can shoot

			maxMissilesRandomNumber = 1000; //decreasing the range whereby a random number is allocated

			for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				noobEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				easyEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				intermediateEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
				tempNumber = setRandomNumber(minMissilesRandomNumber, maxMissilesRandomNumber); 
				advancedEnemy.missile.setMissileRandomNumber(tempNumber); //setting new random number for the missiles
			}

			break;

		case 2:
			//increase the number of lives remaining for the player

			playership1.increaseLife(1);

			break;

		case 3:
			//decrease the number of lives remaining for the player

			playership1.decreaseLife();

			break;

		case 4:
			//increase the player's horizontal speed

			powered = true;
			speed = true;

			break;

		case 5:
			//decrease the player's horizontal speed

			powered = true;
			speed = false;

			break;

		case 6:
			//make the superEnemy appear more often
			minRandomNumber = minRandomNumber/2;
			maxRandomNumber = maxRandomNumber/2;
			break;

			//		case 7:
			//			//all dead enemies are resurrected
			//			for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			//				if (advancedEnemy.getVisibility() == false) {
			//					advancedEnemy.setVisibility(true);
			//				}
			//			}
			//
			//			for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			//				if (intermediateEnemy.getVisibility() == false) {
			//					intermediateEnemy.setVisibility(true);
			//				}
			//			}
			//
			//			for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			//				if (easyEnemy.getVisibility() == false) {
			//					easyEnemy.setVisibility(true);
			//				}
			//			}
			//
			//			for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			//				if (noobEnemy.getVisibility() == false) {
			//					noobEnemy.setVisibility(true);
			//				}
			//			}
			//			break;
		}
	}

	//sets whether the player is playing multiplayer mode or not
	public void setMultiplayerMode(boolean mode) {

		multiplayerMode = mode;
	}

	//resetting game to initial state
	public static void resetGameToBeginning() {

		//resetting player's level to 0
		playerLevel = 0;

		//wanting to reset game
		resetGame = true;

		//makes the barriers from the previous level invisible
		for (Barrier barrier1 : barrier1Array) {
			barrier1.setVisibility(false);
		}

		for (Barrier barrier2 : barrier2Array) {
			barrier2.setVisibility(false);
		}

		for (Barrier barrier3 : barrier3Array) {
			barrier3.setVisibility(false);
		}

		//making all the remaining player missiles invisible
		for (Missiles bulletObject : bulletObjects) {
			if (bulletObject.getVisibility() == true) {
				bulletObject.setVisibility(false);
			}
		}


		//making all the remaining enemy and enemy missiles missiles invisible
		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			if (advancedEnemy.getVisibility() == true) {
				advancedEnemy.setVisibility(false);
			}

			if (advancedEnemy.missile.getVisibility() == true) {
				advancedEnemy.missile.setVisibility(false);
			}
		}

		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			if (intermediateEnemy.getVisibility() == true) {
				intermediateEnemy.setVisibility(false);
			}

			if (intermediateEnemy.missile.getVisibility() == true) {
				intermediateEnemy.missile.setVisibility(false);
			}
		}

		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			if (easyEnemy.missile.getVisibility() == true) {
				easyEnemy.missile.setVisibility(false);
			}

			if (easyEnemy.getVisibility() == true) {
				easyEnemy.setVisibility(false);
			}
		}

		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			if (noobEnemy.missile.getVisibility() == true) {
				noobEnemy.missile.setVisibility(false);
			}

			if (noobEnemy.getVisibility() == true) {
				noobEnemy.setVisibility(false);
			}
		}

		//game has not been completely initialised as it hasn't been through initialiseGame method yet
		hasGameBeenCompletelyInitialised = false;

		//setting visibilities of player to invisible
		player1.setVisibility(false);
		player2.setVisibility(false);


		//stopping track 2 ready for initialization
		Sounds.stopClip2();
	}


	public static void exitOptions() {

		//resetting player's level to 0
		playerLevel = 0;

		//game has not been completely initialised as it hasn't been through initialiseGame method yet
		hasGameBeenCompletelyInitialised = false;

		//setting visibilities of player to invisible
		player1.setVisibility(false);
		player2.setVisibility(false);


		//makes the barriers from the previous level invisible
		for (Barrier barrier1 : barrier1Array) {
			barrier1.setVisibility(false);
		}

		for (Barrier barrier2 : barrier2Array) {
			barrier2.setVisibility(false);
		}

		for (Barrier barrier3 : barrier3Array) {
			barrier3.setVisibility(false);
		}

		//making all the remaining player missiles invisible
		for (Missiles bulletObject : bulletObjects) {
			if (bulletObject.getVisibility() == true) {
				bulletObject.setVisibility(false);
			}
		}

		//making all the remaining enemy and enemy missiles invisible
		for (EnemySpaceShip advancedEnemy : advancedEnemiesArray) {
			if (advancedEnemy.getVisibility() == true) {
				advancedEnemy.setVisibility(false);
			}

			if (advancedEnemy.missile.getVisibility() == true) {
				advancedEnemy.missile.setVisibility(false);
			}
		}

		for (EnemySpaceShip intermediateEnemy : intermediateEnemiesArray) {
			if (intermediateEnemy.getVisibility() == true) {
				intermediateEnemy.setVisibility(false);
			}

			if (intermediateEnemy.missile.getVisibility() == true) {
				intermediateEnemy.missile.setVisibility(false);
			}
		}

		for (EnemySpaceShip easyEnemy : easyEnemiesArray) {
			if (easyEnemy.missile.getVisibility() == true) {
				easyEnemy.missile.setVisibility(false);
			}

			if (easyEnemy.getVisibility() == true) {
				easyEnemy.setVisibility(false);
			}
		}

		for (EnemySpaceShip noobEnemy : noobEnemiesArray) {
			if (noobEnemy.missile.getVisibility() == true) {
				noobEnemy.missile.setVisibility(false);
			}

			if (noobEnemy.getVisibility() == true) {
				noobEnemy.setVisibility(false);
			}
		}
	}
}