import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

public class EnterNameScreen extends JPanel implements ActionListener{

	
	private static final long serialVersionUID = 1L;
	JTextField textfield = new JTextField(17);
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	GameControl state = new GameControl();

	
	public EnterNameScreen() throws IOException{
		this.setBackground(Color.BLACK); 
		
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		
		background.setLayout(new GridBagLayout());
		GridBagConstraints enterNameScreenContraint = new GridBagConstraints();
		enterNameScreenContraint.insets = new Insets(20,0,40,20);
		
		
		JLabel enterNameLabel = new JLabel("<html> Enter Your Name Below <html>", JLabel.CENTER);
		enterNameLabel.setFont(new Font("Aharoni", Font.BOLD, 60));
		enterNameLabel.setForeground(Color.WHITE);
		
		//setting positions on screen for components
		enterNameScreenContraint.gridx = 0;
		enterNameScreenContraint.gridy = 1;
		background.add(enterNameLabel,enterNameScreenContraint);
		
		JButton button = new JButton("Enter");
		button.setFont(new Font("Aharoni", Font.BOLD, 20));
		textfield.setForeground(Color.BLACK);
		textfield.setFont(new Font("Aharoni", Font.BOLD, 35));
		textfield.addActionListener(this);
		
		//setting positions on screen for components
		enterNameScreenContraint.gridx = 0;
		enterNameScreenContraint.gridy = 2;
		background.add(textfield,enterNameScreenContraint);
		
		//setting positions on screen for components
		enterNameScreenContraint.gridx = 0;
		enterNameScreenContraint.gridy = 3;
		background.add(button,enterNameScreenContraint);
		
		button.addActionListener(this);
		
		this.add(background);
		validate();
	}

	@Override
	public void actionPerformed(ActionEvent name) {
		//getText converts JTextfield to string and assigns it to input
		String textFieldInput = textfield.getText();
		try {
			if ((textFieldInput != "\n") || (textFieldInput != " ")){
			//this created a log file if there isn't one already but doesn't flush immediately 
			PrintWriter log = new PrintWriter(new FileWriter("Log file.txt", true));
			//gets the input from the text field and the points from play space ship class
			log.println(textFieldInput+";"+PlayerSpaceship.getPoints());
			//this code flushes to the log file 
			log.flush();
			//this closes the log file ready to be opened or used the next time 
			log.close();
			Screens.goToMainMenu();
			}
		}	
		 catch (IOException e1) {
			e1.printStackTrace();
		}
		//this code stops the song once everything has been flushed to switch it to the next track 
		Sounds.stopClip1();
		try {
			Sounds.playClip2();
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
	}
}

	

