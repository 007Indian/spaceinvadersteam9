import java.awt.Image;

//class containing information about Enemy Missiles
public class PowerUps {
	private Image powerUpImage; //enemyMissilesImage is an object of type Image
	private float xPosition = 0; //x position
	private float yPosition = 0; //y position
	private boolean visible; //visibility
	public int randomNumber;
	public int minYPosition = 10; //minimum y position
	public int maxYPosition = 550; //maximum y position
	//public int speedOfMissiles = 3;
	private double xVelocity = 0; //velocity of x
	private double yVelocity = 2; //speedOfMissiles; //velocity of y

	//constructor for EnemySpaceShip class
	public PowerUps(Image PUImage) {

		this.powerUpImage = PUImage;
	}

	//update position of missile
	public void updatePosition() {
		if ((yPosition <= maxYPosition) & (yPosition >= minYPosition)) {
			yPosition += yVelocity; //so that the missile travels downwards 
		} else {
			visible = false; //making missile invinsible
		}
	}

	//returns x position of missile
	public float getXPosition() {
		return xPosition;
	}

	//returns y position of missile
	public float getYPosition() {
		return yPosition;
	}

	//set x position of missile
	public void setXPosition(float x) {
		this.xPosition = x; //the xPosition of the missile equals the value of x passed
	}

	//set y position of missile
	public void setYPosition(float y) {
		this.yPosition = y; //the yPosition of the missile equals the value of y passed
	}

	//get width of the image
	public int getWidth(){
		return powerUpImage.getWidth(null); //returns the width of the image in pixels
	}

	//get height of the image
	public int getHeight(){
		return powerUpImage.getHeight(null); //returns the height of the image in pixels
	}

	//returns horizontal velocity (xVelocity)
	public double getXVelocity() {
		return xVelocity;
	}

	//returns vertical velocity (yVelocity)
	public double getYVelocity() {
		return yVelocity;
	}

	//set horizontal velocity (xVelocity)
	public void setXVelocity(double xv) {
		this.xVelocity = xv;
	}

	//set vertical velocity (yVelocity)
	public void setYVelocity(double yv) {
		this.yVelocity = yv;
	}

	//get visibility value
	public boolean getVisibility() {
		return visible;
	}

	//set visibility value
	public void setVisibility(boolean b) {
		this.visible = b;
	}

	//get image of missile
	public Image getImage(){
		return powerUpImage;
	}

	//returns random number of missile
	public float getPowerUpRandomNumber() {
		return randomNumber;
	}

	//set x position of missile
	public void setPowerUpRandomNumber(int number) {
		this.randomNumber = number; //the xPosition of the missile equals the value of x passed
	}
}