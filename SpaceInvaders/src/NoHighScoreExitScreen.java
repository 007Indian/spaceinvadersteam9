import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class NoHighScoreExitScreen extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();

	public NoHighScoreExitScreen(){
		
		
		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints NoHighScoreExitScreenContraint = new GridBagConstraints();
		NoHighScoreExitScreenContraint.insets = new Insets(0,0,20,0);


		JLabel exitLabel1 = new JLabel("<html> GAME OVER!<br><br> <html>", JLabel.CENTER);

		exitLabel1.setFont(new Font("Aharoni", Font.BOLD, 70));
		exitLabel1.setForeground(Color.WHITE);

		JLabel exitLabel2 = new JLabel("<html> You lost. Thank you for playing the game.<br><br><br><br><br><br>", JLabel.CENTER);
		exitLabel2.setFont(new Font("Aharoni", Font.PLAIN, 30));
		exitLabel2.setForeground(Color.WHITE);

		NoHighScoreExitScreenContraint.gridx = 0;
		NoHighScoreExitScreenContraint.gridy = 0;
		
		background.add(exitLabel1, NoHighScoreExitScreenContraint);
		
		NoHighScoreExitScreenContraint.gridx = 0;
		NoHighScoreExitScreenContraint.gridy = 1;
		background.add(exitLabel2, NoHighScoreExitScreenContraint);
		


		JButton button9 = new JButton("Main Menu"); 
		button9.setFont(new Font("Aharoni", Font.BOLD, 30));
		button9.addActionListener(this); 
		button9.setForeground(lighterBlue);


		NoHighScoreExitScreenContraint.gridx = 0;
		NoHighScoreExitScreenContraint.gridy = 2;
		background.add(button9,NoHighScoreExitScreenContraint);
		this.add(background);
		
	}

	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Main Menu"))){
			Screens.goToMainMenu();

			state.welcomeScreenEntered(true);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
			
			Sounds.stopClip1();
			try {
				Sounds.playClip2();
			} catch (UnsupportedAudioFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}		
	}		
}
