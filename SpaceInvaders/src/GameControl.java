public class GameControl {

	boolean booleanForWelcomeScreen = true;
	boolean booleanForOnePlayerGameScreen = false;
	boolean booleanForTwoPlayerGameScreen = false;
	boolean booleanForEnterNameScreen = false;
	boolean booleanForHighScoreScreen = false;
	boolean booleanForHelpScreen = false;
	boolean booleanForOptionScreen = false;
	boolean booleanForNoHighScoreExitScreenScreen = false;
	boolean booleanForHighScoreExitScreenScreen = false;
	boolean booleanForBackGroundSong1 = true;
	boolean booleanForBackGroundSong2 = false;
	boolean booleanForHowManyPlayersScreen = false;
	boolean pauseStatus = false; //true means game is paused, false means game is in operation

	//call these functions if you want to let the controller class know that you are in this particular screen
	public void welcomeScreenEntered(boolean bool){
		booleanForWelcomeScreen = bool;
	}
	public void onePlayerGameScreenEntered(boolean bool){
		booleanForOnePlayerGameScreen = bool;
	}
	public void twoPlayerGameScreenEntered(boolean bool){
		booleanForTwoPlayerGameScreen = bool;
	}
	public void enterNameScreenEntered(boolean bool){
		booleanForEnterNameScreen  = bool;
	}
	public boolean highScoresScreenEntered(boolean bool){
		return booleanForHighScoreScreen = bool;
	}
	public boolean helpScreenEntered(boolean bool){
		return booleanForHelpScreen = bool;
	}
	public boolean optionScreenEntered(boolean bool) {
		return booleanForOptionScreen = bool;
	}
	public boolean noHighScoreExitScreenEntered(boolean bool) {
		return booleanForNoHighScoreExitScreenScreen = bool;
	}
	public boolean highScoreExitScreenEntered(boolean bool) {
		return booleanForHighScoreExitScreenScreen = bool;
	}
	public boolean backGroundSong1(boolean bool) {
		return booleanForBackGroundSong1 = bool;
	}
	public boolean backGroundSong2(boolean bool) {
		return booleanForBackGroundSong2 = bool;
	}
	public boolean howManyPlayersScreenEntered(boolean bool) {
		return booleanForHowManyPlayersScreen = bool;
	}

	//Call these functions to get the status of you are in the screen or not
	public boolean statusOfWelcomeScreen(){
		return booleanForWelcomeScreen;
	}
	public boolean statusOfOnePlayerGameScreen(){
		return booleanForOnePlayerGameScreen;
	}
	public boolean statusOfTwoPlayerGameScreen(){
		return booleanForTwoPlayerGameScreen;
	}
	public boolean statusOfEnterNameScreen (){
		return booleanForEnterNameScreen;
	}
	public boolean statusOfHighScoresScreen (){
		return booleanForHighScoreScreen;
	}
	public boolean statusOfHelpScreen (){
		return booleanForHelpScreen;
	}
	public boolean statusOfOptionScreen (){
		return booleanForOptionScreen;
	}
	public boolean statusOfNoHighScoreExitScreen (){
		return booleanForNoHighScoreExitScreenScreen;
	}
	public boolean statusOfHighScoreExitScreen (){
		return booleanForHighScoreExitScreenScreen;
	}
	public boolean statusOfBackGroundSong1 (){
		return booleanForBackGroundSong1;
	}
	public boolean statusOfBackGroundSong2 (){
		return booleanForBackGroundSong2;
	}
	public boolean statusOfHowManyPlayersScreen (){
		return booleanForHowManyPlayersScreen;
	}
	
	//sets the status for pause
	public void setPauseStatus(boolean x) {
		pauseStatus = x;
	}
	
	//returns the status of pause
	public boolean getPauseStatus() {
		return pauseStatus;
	}
	

}


