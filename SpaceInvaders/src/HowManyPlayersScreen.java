import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class HowManyPlayersScreen extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	
	//these colours can be used anywhere in this file
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	GameControl state = new GameControl();



	public HowManyPlayersScreen(){

		this.setBackground(Color.BLACK); 
		
		JLabel background=new JLabel(new ImageIcon("Stars.png"));

		this.setPreferredSize(new Dimension(800, 600));
		this.setBackground(navyBlue);
		this.setLocation(400, 300);


		JLabel question = new JLabel("<html>How many players wish to play? <br><br>", JLabel.CENTER);
		question.setFont(new Font("Aharoni", Font.BOLD, 40));
		question.setForeground(Color.WHITE);


		background.add(question);

		JButton button15 = new JButton("Single Player");
		JButton button16 = new JButton("Multiplayer");

		button15.setFont(new Font("Aharoni", Font.BOLD,30));
		button16.setFont(new Font("Aharoni", Font.BOLD,30));

		button15.addActionListener(this);
		button16.addActionListener(this);


		background.setLayout(new GridBagLayout());
		GridBagConstraints HowManyPlayersConstraint = new GridBagConstraints();
		HowManyPlayersConstraint.insets = new Insets(15,20,15,15);  

		HowManyPlayersConstraint.gridx = 0;
		HowManyPlayersConstraint.gridy = 2;
		background.add(button15,HowManyPlayersConstraint);
		HowManyPlayersConstraint.gridx = 0;
		HowManyPlayersConstraint.gridy = 3;
		background.add(button16,HowManyPlayersConstraint);

		this.add(background);



	}
	//this method is invoked when the single player button is pressed
	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Single Player"))){
			Screens.goToSinglePlayerGameScreen();
			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(true);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);

		}					
		//this method is invoked when the multiplayer button is pressed
		else if ((name.getActionCommand().equals("Multiplayer"))){
			Screens.goToTwoPlayerGameScreen();
			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(true);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(false);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
			
			System.out.println(state.statusOfTwoPlayerGameScreen());

		}					
	}
}

