import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HighScoreExitScreen extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	Color sandyBrown = new Color(244, 164, 96); //pastel dark orange
	Color moccasin = new Color(255, 239, 213); //pastel light yellow
	Color navyBlue = new Color(24, 42, 63); //pastel navy blue
	Color lighterBlue = new Color(147, 178, 214); //pastel lighterBlue blue
	Color babyBlue = new Color(233, 239, 246); //pastel babyBlue blue
	
	GameControl state = new GameControl();

	public HighScoreExitScreen(){

		this.setBackground(Color.BLACK);
		JLabel background=new JLabel(new ImageIcon("Stars.png"));
		background.setLayout(new GridBagLayout());
		GridBagConstraints highScoreExitScreenContraint = new GridBagConstraints();
		highScoreExitScreenContraint.insets = new Insets(0,0,20,0);
		
		//CREATING AND THEN ADDING THE VARIOUS COMPNENTS TO THERE DISTINCT POSITIONS
		JLabel exitLabel3 = new JLabel("<html> GAME OVER! <br><br><html>", JLabel.CENTER);

		exitLabel3.setFont(new Font("Aharoni", Font.BOLD, 70));
		exitLabel3.setForeground(Color.WHITE);

		JLabel exitLabel4 = new JLabel("<html> Congratulations. You have a high score.", JLabel.CENTER);
		exitLabel4.setFont(new Font("Aharoni", Font.PLAIN, 26));
		exitLabel4.setForeground(Color.WHITE);

		JLabel exitLabel5 = new JLabel("<html>Thank you for playing the game<br><br><br><br><br>", JLabel.CENTER);
		exitLabel5.setFont(new Font("Aharoni", Font.PLAIN, 26));
		exitLabel5.setForeground(Color.WHITE);
		
		highScoreExitScreenContraint.gridx = 0;
		highScoreExitScreenContraint.gridy = 0;
		background.add(exitLabel3,highScoreExitScreenContraint);
		
		highScoreExitScreenContraint.gridx = 0;
		highScoreExitScreenContraint.gridy = 1;
	
		background.add(exitLabel4,highScoreExitScreenContraint);
		
		highScoreExitScreenContraint.gridx = 0;
		highScoreExitScreenContraint.gridy = 2;
		background.add(exitLabel5,highScoreExitScreenContraint);
		 
	
		JButton enterNameButton = new JButton("Enter Name");
		enterNameButton.setFont(new Font("Aharoni", Font.BOLD,30));
		enterNameButton.addActionListener(this);
		enterNameButton.setForeground(lighterBlue);

		highScoreExitScreenContraint.gridx = 0;
		highScoreExitScreenContraint.gridy = 3;
		background.add(enterNameButton,highScoreExitScreenContraint);
		
		this.add(background);
		validate();

	}

	@Override
	public void actionPerformed(ActionEvent name) {
		if ((name.getActionCommand().equals("Enter Name"))){
			
			Screens.goToEnterNameScreen();
			
			state.welcomeScreenEntered(false);
			state.onePlayerGameScreenEntered(false);
			state.twoPlayerGameScreenEntered(false);
			state.howManyPlayersScreenEntered(false);
			state.enterNameScreenEntered(true);
			state.highScoresScreenEntered(false);
			state.helpScreenEntered(false);
			state.optionScreenEntered(false);
			state.noHighScoreExitScreenEntered(false);
			state.highScoreExitScreenEntered(false);
			

		}		
	}

}
