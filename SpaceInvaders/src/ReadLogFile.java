import java.io.*;
import java.util.*;

public class ReadLogFile {

	//initialising array lists to be used in this file global variables
	private ArrayList<String> stringOfNames = new ArrayList<String>();
	private ArrayList<String> stringOfHighScores = new ArrayList<String>();
	private ArrayList<Integer> ints = new ArrayList<Integer>();
	private ArrayList<Integer> scores = new ArrayList<Integer>();
	private ArrayList<String> names = new ArrayList<String>();


	@SuppressWarnings({ "rawtypes", "unused" })
	
	//reading the log file
	public ReadLogFile() throws Exception{
		FileReader file = new FileReader("Log File.txt");
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(file);
		String text = "";
		String line = reader.readLine();

		while (line != null){
			if (text!="\n"){
				text +=line + "\n";
				line=reader.readLine();
			}
		}
		
		//splitting splitStringArray by the ";" delimiter
		String[] splitStringArray = text.split(";");


		String [] s2 = splitStringArray;
		for (int j = 0 ; j < splitStringArray.length ; j++)
		{
			//splitting splitStringArray by the ";" delimiter
			s2 = splitStringArray[j].split( "\n" );
			for( int i = 0; i < s2.length; i++)
			{
				if ((i%2!=0)){
					stringOfNames.add(s2 [i]);
				}
			}
		}
		String [] s3 = splitStringArray;
		
		//starting loop at 1 to avoid getting the first line which describes the log file
		for (int j = 1 ; j < splitStringArray.length ; j++)
		{
			s3 = splitStringArray[j].split( "\n" );
			for( int i = 0; i < s3.length; i++)
			{
				if ((i%2==0)){
					stringOfHighScores.add(s3 [i]);
				}
			}
		}


		for (String stringValue : stringOfHighScores) {
			ints.add(Integer.parseInt(stringValue));	

		}
	
		//A HashMap is an array that keeps the elements in it mapped such that if one is changed
		//the other is changed with it in any way
		HashMap<Integer, String> highScoreTableUnsorted = new HashMap<Integer, String>();

		for (int i =0 ; i < stringOfNames.size() ; i++ )
		highScoreTableUnsorted.put(ints.get(i), stringOfNames.get(i));	

		
         Set set = highScoreTableUnsorted.entrySet();
         
         //The understanding behind this code is from http://beginnersbook.com/2013/12/how-to-sort-hashmap-in-java-by-keys-and-values/
         //This sorts out the Scores into ascending order
         Map<Integer, String> map = new TreeMap<Integer, String>(highScoreTableUnsorted); 
         Set sortedSet = map.entrySet();
         Iterator iteratesThroughHighScoreTableUnsorted = sortedSet.iterator();
         
         while(iteratesThroughHighScoreTableUnsorted.hasNext()) {
              Map.Entry finalSortedSet = (Map.Entry)iteratesThroughHighScoreTableUnsorted.next();
          	scores.add((Integer) finalSortedSet.getKey());
			names.add( (String) finalSortedSet.getValue());
         }
    }

	
	//returns array lists ready to be inputed into the JTable in the HIghScoresScreen class
	public ArrayList<String> returnNamesFromLogFile(){
		return names;

	}
	public ArrayList<Integer> returnScoresFromLogFile(){
		return scores;
	}


}


